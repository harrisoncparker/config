<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itemactions', function(Blueprint $table)
		{
			$table->integer('aid');
			$table->integer('rid');
			$table->char('class', 16);
			$table->timestamp('event')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('status')->nullable()->default(0);
			$table->text('message', 65535);
			$table->primary(['aid','rid','class','event']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itemactions');
	}

}
