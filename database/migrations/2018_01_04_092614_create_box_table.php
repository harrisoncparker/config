<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBoxTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('box', function(Blueprint $table)
		{
			$table->integer('boxid', true); // ID
			$table->char('boxname')->default('miranda')->unique('boxname'); // Name
			$table->char('fqdn', 250)->nullable(); // Fully Qualified Domain Name
			$table->integer('lan')->nullable(); // LAN this belongs to
			$table->integer('cluster')->default(0); // Cluster this belongs to (If head = on)
			$table->char('ipv4', 15)->nullable(); // IPv4
			$table->char('user', 64)->nullable()->default('reins'); // The script user
			$table->char('arch', 16)->nullable(); // Architecture // Chip set
			$table->char('os', 16)->nullable(); // Operating System
			$table->char('osver', 16)->nullable(); // Operating System Version
			$table->integer('devhost')->default(1); // Box foreign key. Pointing to compiling server
			$table->char('copy', 64)->default('scp'); // Copy command // get rid of
			$table->char('shell', 64)->default('ssh'); // ssh command // get rid of
			$table->text('path', 65535)->nullable(); // exec. path // $PATH
			$table->char('backuppath', 64)->nullable()->default('/var/backups'); // BU Path // Get rid of
			$table->char('codepath')->default('/code'); // Code Path
			$table->string('homepath')->nullable(); // Home path
			$table->char('csql')->nullable()->default('/usr/bin'); // Where to find the sql stuff // get rid of
			$table->char('bin', 128)->nullable()->default('/usr/bin'); // get rid of
			$table->char('ldap', 128)->nullable()->default('/usr/bin'); // get rid of
			$table->char('sendmail', 128)->nullable()->default('/usr/lib/sendmail'); // Path to send mail
			$table->text('publicKey', 65535)->nullable(); // no longer used on box
			$table->text('sslcertf', 65535)->nullable(); // no longer used on box
			$table->text('sslkeyf', 65535)->nullable(); // no longer used on box
			$table->char('sslproxyipv4', 15)->nullable(); // no longer used on box
			$table->integer('mysqlversion')->nullable(); // useful
			$table->integer('apacheversion')->nullable(); // useful
			$table->char('apachegrp', 64)->nullable()->default('www-data'); // useful
			$table->enum('moduleaware', ['on', ''])->default(''); // Has access to be able to publish and subscribe modules // change to bool
			$table->enum('centralcgi', ['on', ''])->default(''); // no longer used // get rid of // change to bool
			$table->char('dbadminuser', 128)->nullable()->default('dbadmin'); // no longer used // get rid of
			$table->char('dbadminuserpw', 128)->nullable()->default('snooty781film'); // no longer used // get rid of
			$table->enum('active', ['on', ''])->default(''); // change to bool
			$table->enum('fastobyx', ['on', ''])->default(''); // no longer used // get rid of // change to bool
			$table->enum('head', ['on', ''])->default(''); // Belongs to a cluster // Is active head // Actively belongs to cluster // change to bool
			$table->text('notes', 65535)->nullable(); // pos useful
			$table->char('wwwpath')->default('/websites'); // useful // cluster only
			$table->char('scratchpath', 250)->default('/websites/tmp'); // Should belong to services // Where anon user ca write temp files
			$table->char('lockpath', 250)->default('/websites/tmp'); // useful
			$table->char('sitepath', 64)->default('site');
			$table->char('cgipath', 64)->default('/var/www/cgi');
			$table->text('li_cache', 65535)->nullable(); // get rid of
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('box');
	}

}
