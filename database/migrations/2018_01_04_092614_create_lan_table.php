<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLanTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lan', function (Blueprint $table) {
            $table->integer('lanid', true);
            $table->char('lanname')->unique('lanname');
            $table->enum('active', ['on', ''])->default('on');
            $table->enum('access', ['on', ''])->default('on');
            $table->char('ip4')->nullable();
            $table->char('ip4mask')->nullable();
            $table->char('extgw')->nullable();
            $table->char('intgw')->nullable();
            $table->text('notes', 65535)->nullable();
            $table->text('li_cache', 65535)->nullable(); // don't need
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lan');

    }

}
