<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBoxservicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('boxservices', function(Blueprint $table)
		{
			$table->integer('bsid', true);
			$table->integer('srvid'); // service ID
			$table->integer('boxid'); // box ID
			$table->char('uname', 64)->nullable(); // user name
			$table->char('ugroup', 64)->nullable(); // user group
			$table->char('upword', 128)->nullable(); // user password
			$table->char('path', 128)->nullable();
			$table->enum('remote', ['on', ''])->default(''); // change to bool // usable from other boxes
			$table->char('fqdn', 128)->nullable();
			$table->enum('active', ['on', ''])->default(''); // change to bool
			$table->unique(['srvid','boxid'], 'srvid');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('boxservices');
	}

}
