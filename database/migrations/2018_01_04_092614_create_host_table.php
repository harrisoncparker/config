<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHostTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('host', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('site')->default(0);
			$table->char('fqdn', 120);
			$table->char('domain', 120)->default('');
			$table->text('homepage', 65535)->nullable();
			$table->char('ipv4', 16)->default('*');
			$table->string('custom', 64)->default('');
			$table->char('handler404', 200)->default('');
			$table->char('protocol', 16)->nullable()->default('http');
			$table->text('sslkeyf', 65535)->nullable();
			$table->text('sslcertf', 65535)->nullable();
			$table->text('sslchainf', 65535)->nullable();
			$table->text('aliases', 65535)->nullable();
			$table->string('open')->default('');
			$table->string('sslredir')->default('');
			$table->string('ssl_opt')->default('redirect');
			$table->unique(['site','fqdn'], 'site');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('host');
	}

}
