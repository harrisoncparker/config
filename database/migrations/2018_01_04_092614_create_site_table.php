<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSiteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('site', function(Blueprint $table)
		{
			$table->integer('sid', true);
			$table->string('sitename')->default('mysite')->unique('sitename');
			$table->char('urldts', 120)->default('-dts.redsnapper.net');
			$table->char('urlfts', 120)->default('-fts.redsnapper.net');
			$table->char('urldls', 120)->default('-dls.redsnapper.net');
			$table->char('urlfls', 120)->default('-staging.redsnapper.net');
			$table->char('urldtr', 120)->default('-dtr.redsnapper.net');
			$table->char('urlftr', 120)->default('-test.redsnapper.net');
			$table->char('urldlr', 120)->default('-draft.redsnapper.net');
			$table->char('urlflr', 120)->default('-unused-');
			$table->char('urledt', 120)->default('-edit.redsnapper.net');
			$table->string('use8182')->default('');
			$table->string('validxml')->default('');
			$table->decimal('defaultobyxversion', 8, 6)->default(1.110120);
			$table->char('obyxdmime', 80)->nullable()->default('text/html');
			$table->char('protocol', 16)->nullable()->default('http');
			$table->text('sslcertf', 65535)->nullable();
			$table->text('sslkeyf', 65535)->nullable();
			$table->char('sslchainf', 128)->nullable();
			$table->string('urlp')->nullable()->default('mysite.redsnapper.net');
			$table->string('urla')->default('');
			$table->string('urld')->default('');
			$table->char('urlpdev', 80)->default('');
			$table->char('urlddev', 80)->default('');
			$table->char('prevhome', 120)->default('/1/home.html');
			$table->string('securehome')->default('/x/ua.cgi');
			$table->char('edithome', 250)->default('/mortar/dashboard.obyx');
			$table->string('folder')->default('mysitef');
			$table->string('ldapou', 64)->nullable();
			$table->char('ldapeditorcn', 64)->default('editors');
			$table->char('ldapadmincn', 64)->default('admin');
			$table->string('defaultlang')->default('en');
			$table->string('defaultenc')->default('UTF-8');
			$table->string('contentneg')->default('on');
			$table->char('ipp', 16)->default('[IPv4_NOTSET]');
			$table->string('ipa')->nullable();
			$table->string('ipd')->nullable();
			$table->integer('sitebox')->default(1);
			$table->integer('backupbox')->nullable();
			$table->string('backitup')->default('');
			$table->string('keepstats')->default('');
			$table->string('backitup_sup')->default('');
			$table->string('active')->default('on');
			$table->string('webapp')->default('');
			$table->string('uselang')->default('');
			$table->string('usetech')->default('on');
			$table->string('isnvh')->default('on');
			$table->string('hasadminldap')->default('');
			$table->string('haseditorial')->default('on');
			$table->string('ipup')->default('');
			$table->char('status', 32)->nullable()->default('live');
			$table->char('obyxtag', 80)->nullable()->default('version1');
			$table->string('obyxdev')->default('on');
			$table->integer('obyxhtml')->default(1);
			$table->string('usesmedsorc')->default('');
			$table->char('salt', 80)->nullable();
			$table->string('rstsubject', 128)->nullable()->default('Password Reset (-SiteWW-)');
			$table->string('rstfrom', 128)->nullable()->default('Site Automaton <admin@redsnapper.net>');
			$table->string('rstuser', 128)->nullable()->default('Your Username is :');
			$table->string('rstpasswd', 128)->nullable()->default('Your Password has been reset to :');
			$table->text('rstextra', 65535)->nullable();
			$table->string('cgi', 64)->default('x');
			$table->string('cgidir', 64)->default('bin');
			$table->string('cgistatic')->default('');
			$table->integer('rsqlhost')->default(0);
			$table->char('sqlblddb', 80)->default('mysitedb');
			$table->char('sqlappdb', 80)->default('mysite_app_db');
			$table->char('sqldevdb', 80)->default('mysite_dev_db');
			$table->integer('sqlhost')->default(0);
			$table->integer('sqlport')->default(3306);
			$table->string('sqluser', 64)->default('web');
			$table->string('sqluserpw', 64)->nullable()->default('');
			$table->string('ismirrored')->default('');
			$table->integer('githost')->nullable();
			$table->string('public', 200)->default('');
			$table->string('preview', 200)->default('');
			$table->char('rxlease', 200)->default('');
			$table->char('staging', 200)->default('');
			$table->string('open')->default('');
			$table->string('custompath', 250)->nullable();
			$table->string('rstech', 60)->nullable();
			$table->string('rstech2', 60)->nullable()->default('');
			$table->string('rspoc', 60)->nullable();
			$table->string('cpoc', 60)->nullable();
			$table->string('cphone', 60)->nullable();
			$table->string('cemail', 60)->nullable();
			$table->integer('currentjob')->nullable();
			$table->string('p3phead', 250)->default('');
			$table->string('pcustvirt', 64)->default('');
			$table->string('dcustvirt', 64)->default('');
			$table->string('acustvirt', 64)->default('');
			$table->string('xcustvirt', 64)->default('');
			$table->text('srvalias', 65535);
			$table->float('mortarrev', 10, 0)->default(0);
			$table->string('genvirttested')->default('on');
			$table->text('notes', 65535)->nullable();
			$table->string('404handler', 128)->nullable()->default('"Page not found (404)"');
			$table->string('sqledit', 64)->default('editor');
			$table->string('sqleditpw', 64)->nullable();
			$table->string('nosslredirect')->default('');
			$table->text('podioitem', 65535);
			$table->integer('podiokb')->default(0);
			$table->text('li_cache', 65535)->nullable();
			$table->integer('default_host')->default(0);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('site');
	}

}
