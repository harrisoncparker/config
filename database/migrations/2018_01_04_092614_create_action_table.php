<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('action', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->char('class', 16);
			$table->char('scope', 16);
			$table->char('sig', 64);
			$table->char('name', 64);
			$table->char('command', 128);
			$table->integer('seq')->default(0);
			$table->string('active')->default('');
			$table->text('notes', 65535)->nullable();
			$table->text('li_cache', 65535)->nullable(); // don't need
			$table->index(['active','id'], 'active');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('action');
	}

}
