<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOtagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('otags', function(Blueprint $table)
		{
			$table->integer('boxid');
			$table->char('tag', 80)->default('');
			$table->text('comment', 65535)->nullable();
			$table->primary(['boxid','tag']);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('otags');
	}

}
