<?php

use App\Lan;
use Faker\Generator as Faker;

$factory->define(Lan::class, function (Faker $faker) {
    return [
        'lanname' => $faker->streetSuffix.$faker->lastName,
        'active' => 'on',
        'access' => '',
        'ip4' => $faker->ipv4,
        'ip4mask' => $faker->ipv4,
        'extgw' => $faker->ipv4,
        'intgw' => $faker->ipv4,
        'notes' => $faker->paragraph(2)
    ];
});
