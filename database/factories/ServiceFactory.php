<?php

use App\Service;
use Faker\Generator as Faker;

$factory->define(Service::class, function (Faker $faker) {
    return [
        'srvname' => $faker->streetSuffix.$faker->lastName,
        'active' => 'on',
        'notes' => $faker->paragraph(2)
    ];
});
