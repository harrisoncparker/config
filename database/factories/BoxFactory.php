<?php

use App\Box;
use App\Lan;
use Faker\Generator as Faker;

$factory->define(Box::class, function (Faker $faker) {
    // Generate Box name
    $name = $faker->domainWord;
    // get available lan ids
    $lanIds = Lan::all()->pluck('lanid')->toArray();
    // Return valid Box
    return [
        'boxname' => $name . $faker->streetSuffix,
        'lan' => $faker->randomElement($lanIds),
        'fqdn' => "$name.redsnapper.net",
        'ipv4' => $faker->ipv4,
        'active' => 'on',
        'notes' => $faker->paragraph(2)
    ];
});
