<?php

use App\Action;
use Faker\Generator as Faker;

$factory->define(Action::class, function (Faker $faker) {
    // Get instance of the action model
    $action = new Action;
    // generate active action using faker
    return [
        'name' =>  $faker->lastName . $faker->streetSuffix,
        'class' => $faker->randomElement($action->getClassValues()),
        'scope' => $faker->randomElement($action->getScopeValues()),
        'sig' => "pfx_$faker->word",
        'command' => "reins $faker->word $faker->word",
        'active' => 'on',
        'notes' => $faker->paragraph(2)
    ];
});
