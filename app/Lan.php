<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lan extends Model
{
    /** @var string  */
    protected $table = 'lan';
    /** @var string  */
    protected $primaryKey = 'lanid';
    /** @var array  */
    protected $fillable = [
        'lanname',
        'active',
        'access',
        'ip4',
        'ip4mask',
        'extgw',
        'intgw',
        'notes'
    ];
    /** @var array  */
    protected $casts = [
        'active' => 'boolean',
        'access' => 'boolean',
    ];

    /**
     * @return string
     */
    public function activeClass()
    {
        return $this->active ? 'active' : 'inactive';
    }

    /**
     * @return mixed
     */
    public static function options()
    {
        return with(new static)->orderBy('lanname')->pluck('lanname', 'lanid');
    }

    /**
     * @return mixed
     */
    public function getActions()
    {
        return Action::where('class', $this->table)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function boxes()
    {
        return $this->hasMany('App\Box', 'lan');
    }

    /**
     * @return int
     */
    public function boxesCount()
    {
        return $this->boxes->count();
    }

}