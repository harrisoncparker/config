<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Action extends Model
{
    /** @var string  */
    protected $table = 'action';
    /** @var array  */
    protected $fillable = [
        'name',
        'class',
        'scope',
        'sig',
        'command',
        'active',
        'notes'
    ];
    /** @var array  */
    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * Get class string active status
     * @return string
     */
    public function activeClass(): string
    {
        return $this->active ? 'active' : 'inactive';
    }

    /**
     * @return array
     */
    public function getClassValues(): array
    {
        $site = new Site;
        $box = new Box;
        $lan = new Lan;

        return [
            $site->getTable(),
            $box->getTable(),
            $lan->getTable()
        ];
    }

    /**
     * @return Collection
     */
    public function getClassOptions(): Collection
    {
        return collect($this->getClassValues())->mapWithKeys(function ($value) {
            return [$value => $value];
        });
    }

    /**
     * @return array
     */
    public function getScopeValues(): array
    {
        return [
            'init',
            'maint',
            'status',
            'action',
            'teardown'
        ];
    }

    /**
     * @return Collection
     */
    public function getScopeOptions(): Collection
    {
        return collect($this->getScopeValues())->mapWithKeys(function ($value) {
            return [$value => $value];
        });
    }

}
