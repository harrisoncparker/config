<?php

namespace App\Http\Controllers\Service;

use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Formlets\Service\ServiceForm;

class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::all();

        return view('entities.service.index', [
            'services' => $services,
            "title" => "List of Services"
        ]);
    }

    public function create(ServiceForm $form)
    {
        return $form->renderWith([
            'route' => ['services.store']
        ])->with([
            "title" => "Create Service form"
        ]);
    }

    public function edit(Service $service, ServiceForm $form)
    {
        $form->setModel($service);
        $form->setFormView("entities.service.edit");

        return $form->renderWith([
            'route' => ['services.update', $service],
            'method' => 'PATCH'
        ])->with([
            "title" => "Edit Service: $service->srvname",
            "service" => $service
        ]);
    }

    public function store(ServiceForm $form)
    {
        $service = $form->store();
        flash("A new Service called $service->srvname has been created.");
        return redirect()->route('services.index');
    }

    public function update(Service $service, ServiceForm $form)
    {
        // Set correct Service to update
        $form->setModel($service);
        // Update Service
        $form->update();
        // notify
        flash("The Service $service->srvname has been updated.");
        // Redirect back to
        return redirect()->route('services.edit', ['service' => $service]);
    }

    public function destroy(Service $service)
    {
        if ($service->active) {
            flash("You cannot delete an active Service.")->error();
            return redirect()->route('services.edit', $service);
        } else {
            Service::destroy($service->srvid);
            flash("The Service $service->srvname has been deleted.");
            return redirect()->route('services.index');
        }
    }
}
