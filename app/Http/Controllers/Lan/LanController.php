<?php

namespace App\Http\Controllers\Lan;

use App\Lan;
use App\Http\Formlets\Lan\LanForm;
use App\Http\Controllers\Controller;

class LanController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $lans = Lan::all();

        return view('entities.lan.index', [
            'lans' => $lans,
            "title" => "List of LANs"
        ]);

    }

    public function create(LanForm $form)
    {
        return $form->renderWith([
            'route' => ['lans.store']
        ])->with([
            "title" => "Create LAN form"
        ]);
    }

    public function edit(Lan $lan, LanForm $form)
    {

        $form->setModel($lan);
        $form->setFormView("entities.lan.edit");

        return $form->renderWith([
            'route' => ['lans.update', $lan],
            'method' => 'PATCH'
        ])->with([
            "title" => "Edit LAN: $lan->lanname",
            "lan" => $lan
        ]);

    }

    public function store(LanForm $form)
    {
        $lan = $form->store();
        flash("A new Lan called $lan->lanname has been created.");
        return redirect()->route('lans.index');
    }

    public function update(Lan $lan, LanForm $form)
    {
        // Set correct Lan to update
        $form->setModel($lan);
        // Update Lan
        $form->update();
        // notify
        flash("The Lan $lan->lanname has been updated.");
        // Redirect back to
        return redirect()->route('lans.edit', ['lan' => $lan]);
    }

    public function destroy(Lan $lan)
    {
        if ($lan->active) {
            flash("You cannot delete an active LAN.")->error();
            return redirect()->route('lans.edit', $lan);
        } elseif ($lan->boxes->count()) {
            flash("You cannot delete a LAN that has boxes assigned to it.")->error();
            return redirect()->route('lans.edit', $lan);
        } else {
            Lan::destroy($lan->lanid);
            flash("The Lan $lan->lanname has been deleted.");
            return redirect()->route('lans.index');
        }
    }

}
