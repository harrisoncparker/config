<?php

namespace App\Http\Controllers\Lan;

use App\Lan;
use App\Action;
use App\ItemAction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class LanActionController extends Controller
{
    public function show(Lan $lan, Action $action)
    {

        // make sure this is a LAN Action
        if ($action->class != "lan")
            App::abort(404);

        // Get ItemAction history
        $history = ItemAction::where("aid", $action->id)
            ->where("rid", $lan->lanid)
            ->orderBy("event", "desc")
            ->get();
        // organise data
        $dataToBeShown = [
            "lan" => $lan,
            "backButton" => [
                "text" => "Edit: $lan->lanname",
                "route" => route("lans.edit", [
                    "lan" => $lan->lanid
                ])
            ],
            "executeButton" => [
                "route" => route("lans.actions.execute",[
                    "lan" => $lan,
                    "action" => $action
                ])
            ],
            "history" => $history,
            "title" => "$action->name for lan $lan->lanname",
            "command" => $action->command,
            "description" => $action->notes
        ];

        // return the Action show view
        return view("entities.action.show", $dataToBeShown);
    }

    public function execute(Lan $lan, Action $action)
    {
        flash("Command: $action->command");

        $status = 0;
        $massage = "This is an example command message.";

        $this->store($lan, $action, $status, $massage);

        return back();
    }

    private function store(Lan $lan, Action $action, $status, $massage)
    {
        $itemaction = ItemAction::create([
            "aid" => $action->id,
            "rid" => $lan->lanid,
            "class" => "lan",
            "status" => $status,
            "message" => $massage
        ]);
    }
}
