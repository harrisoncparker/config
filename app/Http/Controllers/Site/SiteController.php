<?php

namespace App\Http\Controllers\Site;

use App\Site;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $siteName = 'test';

        return json_encode([
            'sitename' => $siteName,
            'urldts' => '-dts.redsnapper.net',
            'urlfts' => '-fts.redsnapper.net',
            'urldls' => '-dls.redsnapper.net',
            'urlfls' => '-staging.redsnapper.net',
            'urldtr' => '-dtr.redsnapper.net',
            'urlftr' => '-test.redsnapper.net',
            'urldlr' => '-draft.redsnapper.net',
            'urlflr' => '-unused-',
            'urledt' => '-edit.redsnapper.net'
        ]);



        //  insert into confsite set
        //	sitename='[sitename]',sqluserpw='[sqluserpw]',sqleditpw='[sqleditpw]',
        //	folder='[sitename]_f',sqlblddb='[sitename]_bld_d',sqldevdb='[sitename]_dev_d',sqlappdb='[sitename]_app_d',sqluser='[sitename]_u',sqledit='[sitename]_e',sqlport='3306',
        //	urlp='[sitename].redsnapper.net',urld='[sitename]-preview.redsnapper.net',urla='[sitename]-edit.redsnapper.net',
        //	urlpdev='[sitename]-dev.redsnapper.net',urlddev='[sitename]-predev.redsnapper.net',
        //	currentjob='[currentjob]',defaultlang='[defaultlang]',rstech='[rstech]',rstech2='[rstech2]',
        //	rspoc='[rspoc]',cpoc='[cpoc]',cphone='[cphone]',cemail='[cemail]',webapp='[webapp]',
        //	active='[active]',notes='[notes]',status='new',defaultenc='UTF-8',ldapou='RS',validxml='on',usetech='on',
        //	uselang='',use8182='',ipp='*',ipa='*',ipd='*',open='',genvirttested='on',
        //	haseditorial='on',backitup='on',isnvh='',edithome='/mortar/dashboard.obyx',backitup_sup='',keepstats='',hasadminldap='',
        //	obyxhtml='2',obyxdev='on',defaultobyxversion='1.120101',protocol='https'
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Site $site
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Site $site
     * @return \Illuminate\Http\Response
     */
    public function edit(Site $site)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Site $site
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Site $site)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Site $site
     * @return \Illuminate\Http\Response
     */
    public function destroy(Site $site)
    {
        //
    }
}
