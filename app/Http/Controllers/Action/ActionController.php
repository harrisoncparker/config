<?php

namespace App\Http\Controllers\Action;

use App\Action;
use App\Http\Formlets\Action\ActionForm;
use App\Http\Controllers\Controller;

class ActionController extends Controller
{

    public function index()
    {
        $actions = Action::all();

        return view('entities.action.index', [
            'actions' => $actions,
            "title" => "List of Actions"
        ]);
    }

    public function create(ActionForm $form)
    {
        return $form->renderWith([
            'route' => ['actions.store']
        ])->with([
            "title" => "Create Action form"
        ]);
    }

    public function edit(Action $action, ActionForm $form)
    {
        $form->setModel($action);
        $form->setFormView("entities.action.edit");

        return $form->renderWith([
            'route' => ['actions.update', $action],
            'method' => 'PATCH'
        ])->with([
            "title" => "Edit Action: $action->name",
            "action" => $action
        ]);
    }

    public function store(ActionForm $form)
    {
        $action = $form->store();
        flash("A new Action called $action->name has been created.");
        return redirect()->route('actions.index');
    }

    public function update(Action $action, ActionForm $form)
    {
        // Set correct Action to update
        $form->setModel($action);
        // Update Action
        $form->update();
        // notify
        flash("The Action $action->name has been updated.");
        // Redirect back to
        return redirect()->route('actions.edit', ['action' => $action]);
    }

    public function destroy(Action $action)
    {
        if ($action->active) {
            flash("You cannot delete and active action.")->error();
            return redirect()->route('actions.edit', $action);
        } else {
            Action::destroy($action->id);
            flash("The Action $action->name has been deleted.");
            return redirect()->route('actions.index');
        }
    }

}
