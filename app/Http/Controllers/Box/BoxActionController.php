<?php

namespace App\Http\Controllers\Box;

use App\Box;
use App\Action;
use App\ItemAction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class BoxActionController extends Controller
{
    public function show(Box $box, Action $action)
    {

        // make sure this is a Box Action
        if ($action->class != "box")
            App::abort(404);
        // Get ItemAction history
        $history = ItemAction::where("aid", $action->id)
            ->where("rid", $box->boxid)
            ->orderBy("event", "desc")
            ->get();
        // organise data
        $dataToBeShown = [
            "box" => $box,
            "backButton" => [
                "text" => "Edit: $box->boxname",
                "route" => route("boxes.edit", [
                    "box" => $box->boxid
                ])
            ],
            "executeButton" => [
                "route" => route("boxes.actions.execute",[
                    "box" => $box,
                    "action" => $action
                ])
            ],
            "history" => $history,
            "title" => "$action->name for box $box->boxname",
            "command" => $action->command,
            "description" => $action->notes
        ];
        // return the Action show view
        return view("entities.action.show", $dataToBeShown);
    }

    public function execute(Box $box, Action $action)
    {
        flash("Command: $action->command");

        $status = 0;
        $massage = "This is an example command message.";

        $this->store($box, $action, $status, $massage);

        return back();
    }

    private function store(Box $box, Action $action, $status, $massage)
    {
        $itemaction = ItemAction::create([
            "aid" => $action->id,
            "rid" => $box->boxid,
            "class" => "box",
            "status" => $status,
            "message" => $massage
        ]);
    }
}
