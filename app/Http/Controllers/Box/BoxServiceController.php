<?php

namespace App\Http\Controllers\Box;

use App\Box;
use App\Service;
use App\BoxService;
use App\Http\Formlets\Box\BoxServiceEditForm;
use App\Http\Formlets\Box\BoxServiceCreateForm;
use App\Http\Controllers\Controller;

class BoxServiceController extends Controller
{

    public function index(Box $box)
    {
        // get Box Service forms
        $boxServiceEditForms = BoxService::where('boxid', $box->boxid)
            ->get()
            ->mapWithKeys(function ($item) {
                // initiate a form
                $form = app(BoxServiceEditForm::class);
                // set the forms model
                $form->setModel($item);
                // return NView object
                return [
                    Service::find($item->srvid)->srvname => $form->renderWith([
                        'route' => ['boxservices.update', $item],
                        'method' => 'PATCH'
                    ])
                ];
            });
        // get box service create form
        $boxServiceCreateForm = app(BoxServiceCreateForm::class, ["box" => $box])->renderWith([
            'route' => ['boxservices.store'],
            'method' => 'POST'
        ]);
        // return view with data
        return view('entities.box.service', [
            'title' => "Services for $box->boxname",
            'box' => $box,
            'boxServiceEditForms' => $boxServiceEditForms,
            'boxServiceCreateForm' => $boxServiceCreateForm
        ]);
    }

    public function store(BoxServiceCreateForm $form)
    {
        $boxService = $form->store();
        $box = Box::find($boxService->boxid);
        $service = Service::find($boxService->srvid);
        flash("The Service $service->srvname has been added to the Box $box->boxname.");
        return redirect()->route('boxes.services.index', ['box' => $box]);
    }

}
