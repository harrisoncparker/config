<?php

namespace App\Http\Controllers\Box;

use App\Box;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Formlets\Box\BoxCreateForm;
use App\Http\Formlets\Box\BoxEditForm;

class BoxController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $boxes = Box::with('boxLan')->get();

        return view('entities.box.index', [
            'boxes' => $boxes,
            "title" => "List of Boxes"
        ]);
    }

    public function create(BoxCreateForm $form)
    {
        return $form->renderWith([
            'route' => ['boxes.store']
        ])->with([
            "title" => "Create Boxes form"
        ]);
    }

    public function edit(Box $box, BoxEditForm $form)
    {
        $form->setModel($box);

        return $form->renderWith([
            'route' => ['boxes.update', $box],
            'method' => 'PATCH'
        ])->with([
            "title" => "Edit Box: $box->boxname",
            "box" => $box
        ]);
    }

    public function store(BoxCreateForm $form)
    {
        $box = $form->store();
        flash("A new Box called $box->boxname has been created.");
        return redirect()->route('boxes.index');
    }


    public function update(BoxEditForm $form, Box $box)
    {
        // Set correct Box to update
        $form->setModel($box);
        // Update Box
        $form->update();
        // notify
        flash("The Box $box->boxname has been updated.");
        // Redirect back to
        return redirect()->route('boxes.edit', ['box' => $box]);
    }

    public function destroy(Box $box)
    {
        if ($box->active) {
            flash("You cannot delete and active Box.")->error();
            return redirect()->route('boxes.edit', $box);
        } else {
            Box::destroy($box->boxid);
            flash("The Box $box->boxname has been deleted.");
            return redirect()->route('boxes.index');
        }
    }
}
