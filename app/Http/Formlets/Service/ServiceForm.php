<?php

namespace App\Http\Formlets\Service;

use App\Service;
use RS\Form\Fields\Checkbox;
use RS\Form\Formlet;
use RS\Form\Fields\Input;
use RS\Form\Fields\TextArea;
use Illuminate\Validation\Rule;

class ServiceForm extends Formlet
{

    public $formView = "entities.service.create";

    public function setFormView($view): void
    {
        $this->formView = $view;
    }

    public function __construct(Service $lan)
    {
        $this->setModel($lan);
    }

    public function prepareForm(): void
    {
        $this->add(
            with(new Input('text', 'srvname'))
                ->setLabel('Service name')
                ->setRequired()
        );

        $this->add(
            with(new Checkbox('active', 'on', ''))
                ->setLabel('Active')
                ->setDefault(true)
        );

        $this->add(
            with(new TextArea('notes'))
                ->setLabel('Notes')
                ->setRows(4)
        );
    }

    public function rules(): array
    {
        return [
            'srvname' => [
                'required',
                'string',
                Rule::unique('service')->ignore($this->model->srvid, 'srvid')
            ]
        ];
    }

}