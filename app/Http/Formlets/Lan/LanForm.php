<?php

namespace App\Http\Formlets\Lan;

use App\Lan;
use RS\Form\Fields\Checkbox;
use RS\Form\Formlet;
use RS\Form\Fields\Input;
use RS\Form\Fields\TextArea;
use Illuminate\Validation\Rule;

class LanForm extends Formlet
{

    public $formView = "entities.lan.create";

    public function setFormView($view): void
    {
        $this->formView = $view;
    }

    public function __construct(Lan $lan)
    {
        $this->setModel($lan);
    }

    public function prepareForm(): void
    {

        $this->add(
            with(new Input('text', 'lanname'))
                ->setLabel('LAN name')
                ->setRequired()
        );

        $this->add(
            with(new Checkbox('active', 'on', ''))
                ->setLabel('Active')
                ->setDefault(true)
        );

        $this->add(
            with(new Checkbox('access', 'on', ''))
                ->setLabel('May Access Services')
                ->setDefault(true)
        );

        $this->add(
            with(new Input('text', 'ip4'))
                ->setLabel('ip4')
                ->setRequired()
        );

        $this->add(
            with(new Input('text', 'ip4mask'))
                ->setLabel('ip4mask')
        );

        $this->add(
            with(new Input('text', 'extgw'))
                ->setLabel('extgw')
        );

        $this->add(
            with(new Input('text', 'intgw'))
                ->setLabel('intgw')
        );

        $this->add(
            with(new TextArea('notes'))
                ->setLabel('Notes')
                ->setRows(4)
        );

    }

    public function rules(): array
    {
        return [
            'lanname' => [
                'required',
                'string',
                Rule::unique('lan')->ignore($this->model->lanid, 'lanid')
            ],
            'ip4' => 'required|string|ipv4',
            'ip4mask' => 'required|string|ipv4',
        ];
    }


}