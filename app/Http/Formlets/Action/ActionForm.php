<?php

namespace App\Http\Formlets\Action;

use App\Action;
use RS\Form\Fields\Checkbox;
use RS\Form\Formlet;
use RS\Form\Fields\Input;
use RS\Form\Fields\Select;
use RS\Form\Fields\TextArea;
use Illuminate\Validation\Rule;

class ActionForm extends Formlet
{

    public $formView = "entities.action.create";

    public function setFormView($view): void
    {
        $this->formView = $view;
    }

    public function __construct(Action $action)
    {
        $this->setModel($action);
    }

    public function prepareForm(): void
    {
        $this->add(
            with(new Input('text', 'name'))
                ->setLabel('Action name')
                ->setRequired()
        );

        $this->add(
            with(new Select('class', $this->model->getClassOptions()))
                ->setLabel('Class')
                ->setRequired()
        );

        $this->add(
            with(new Select('scope', $this->model->getScopeOptions()))
                ->setLabel('Scope')
                ->setRequired()
        );

        $this->add(
            with(new Input('text', 'sig'))
                ->setLabel('Sig')
                ->setRequired()
        );

        $this->add(
            with(new Input('text', 'command'))
                ->setLabel('Command')
                ->setRequired()
        );

        $this->add(
            with(new Checkbox('active', 'on', ''))
                ->setLabel('Active')
                ->setDefault(true)
        );

        $this->add(
            with(new TextArea('notes'))
                ->setLabel('Notes')
                ->setRows(4)
        );

    }

    public function rules(): array
    {
//        dd($this->model->command);
        return [
            'name' => [
                'required',
                'string',
                Rule::unique('action', 'name')->ignore($this->model->id)
            ],
            'class' => [
                'required',
                'string',
                Rule::in($this->model->getClassValues())
            ],
            'scope' => [
                'required',
                'string',
                Rule::in($this->model->getScopeValues())
            ],
            'sig' => [
                'required',
                'string',
                Rule::unique('action', 'sig')->ignore($this->model->id)
            ],
            'command' => [
                'required',
                'string',
                Rule::unique('action', 'command')->ignore($this->model->id)
            ]
        ];
    }
}