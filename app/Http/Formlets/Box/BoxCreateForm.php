<?php

namespace App\Http\Formlets\Box;

use App\Box;
use App\Lan;
use RS\Form\Fields\Checkbox;
use RS\Form\Formlet;
use RS\Form\Fields\Input;
use RS\Form\Fields\Select;
use RS\Form\Fields\TextArea;
use Illuminate\Validation\Rule;

class BoxCreateForm extends Formlet
{
    public $formView = "entities.box.create";

    public function __construct(Box $box)
    {
        $this->setModel($box);
    }

    public function prepareForm(): void
    {
        $this->add(
            with(new Input('text', 'boxname'))
                ->setLabel('Box name')
                ->setRequired()
        );

        $this->add(
            with(new Select('lan', Lan::options()))
                ->setLabel('LAN')
                ->setRequired()
        );

        $this->add(
            with(new Input('text', 'fqdn'))
                ->setLabel('FQDN')
                ->setPlaceholder('boxname.redsnapper.net')
                ->setRequired()
        );

        $this->add(
            with(new Input('text', 'ipv4'))
                ->setLabel('IPv4')
                ->setRequired()
        );

        $this->add(
            with(new Checkbox('active', 'on', ''))
                ->setLabel('Active')
                ->setDefault(true)
        );

        $this->add(
            with(new TextArea('notes'))
                ->setLabel('Notes')
                ->setRows(4)
        );

    }

    public function rules(): array
    {
        return [
            'boxname' => [
                'required',
                'string',
                Rule::unique('box')->ignore($this->model->boxid, 'boxid')
            ],

            'lan' => 'required|exists:lan,lanid',
            'fqdn' => [
                'required',
                Rule::unique('box')->ignore($this->model->boxid, 'boxid')
            ]
        ];
    }

}