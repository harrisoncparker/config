<?php

namespace App\Http\Formlets\Box;

use App\BoxService;
use RS\Form\Formlet;
use RS\Form\Fields\Input;
use RS\Form\Fields\Checkbox;

class BoxServiceEditForm extends Formlet
{

    public $formView = "entities.box.forms.service";

    public function __construct(BoxService $boxService)
    {
        $this->setModel($boxService);
    }

    public function prepareForm(): void
    {

        $this->add(
            with(new Input('text', 'uname'))
                ->setLabel('Service Username')
        );

        $this->add(
            with(new Input('text', 'ugroup'))
                ->setLabel('Service User Group')
        );

        $this->add(
            with(new Input('text', 'upword'))
                ->setLabel('Service User Password')
        );

        $this->add(
            with(new Input('text', 'path'))
                ->setLabel('Service Path')
        );

        $this->add(
            with(new Input('text', 'fqdn'))
                ->setLabel('FQDN')
        );

        $this->add(
            with(new Checkbox('remote', 'on', ''))
                ->setLabel('Usable from other boxes')
        );

        $this->add(
            with(new Checkbox('active', 'on', ''))
                ->setLabel('Active')
                ->setDefault(true)
        );

    }

}
