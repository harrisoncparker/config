<?php

namespace App\Http\Formlets\Box;

use App\Box;
use App\Lan;
use RS\Form\Fields\Checkbox;
use RS\Form\Formlet;
use RS\Form\Fields\Input;
use RS\Form\Fields\Select;
use RS\Form\Fields\TextArea;
use Illuminate\Validation\Rule;

class BoxEditForm extends Formlet
{
    public $formView = "entities.box.edit";

    public function __construct(Box $box)
    {
        $this->setModel($box);
    }

    public function prepareForm(): void
    {
        $this->add(
            with(new Input('text', 'boxname'))
                ->setLabel('Box name')
                ->setRequired()
        );

        $this->add(
            with(new Select('lan', Lan::options()))
                ->setLabel('LAN')
                ->setRequired()
        );

        $this->add(
            with(new Select('devhost', Box::options()))
                ->setLabel('Dev')
                ->setRequired()
        );

        if($this->model->head){
            $this->add(
                with(new Select('cluster', Box::clusterOptions()))
                    ->setLabel('Cluster')
                    ->setRequired()
            );
        }

        $this->add(
            with(new Checkbox('head', 'on', ''))
                ->setLabel('Belongs to cluster')
                ->setDefault(false)
        );

        $this->add(
            with(new Checkbox('active', 'on', ''))
                ->setLabel('Active')
                ->setDefault(true)
        );


        if(!$this->model->head){
            $this->add(
                with(new Checkbox('moduleaware', 'on', ''))
                    ->setLabel('Module Aware')
                    ->setDefault(false)
            );

            $this->add(
                with(new Checkbox('centralcgi', 'on', ''))
                    ->setLabel('Uses Central CGI')
                    ->setDefault(false)
            );

            $this->add(
                with(new Checkbox('fastobyx', 'on', ''))
                    ->setLabel('Uses Fast Obyx')
                    ->setDefault(false)
            );
        }

        $this->add(
            with(new Input('text', 'fqdn'))
                ->setLabel('FQDN')
                ->setPlaceholder('boxname.redsnapper.net')
                ->setRequired()
        );

        $this->add(
            with(new Input('text', 'ipv4'))
                ->setLabel('IPv4')
        );

        if(!$this->model->head){
            $this->add(
                with(new Input('text', 'user'))
                    ->setLabel('User')
                    ->setRequired()
            );
            $this->add(
                with(new Input('text', 'wwwpath'))
                    ->setLabel('www dir')
                    ->setRequired()
            );
            $this->add(
                with(new Input('text', 'sitepath'))
                    ->setLabel('sites dir')
                    ->setRequired()
            );
            $this->add(
                with(new Input('text', 'cgipath'))
                    ->setLabel('cgi dir')
                    ->setRequired()
            );
            $this->add(
                with(new Input('text', 'arch'))
                    ->setLabel('Architecture')
            );
            $this->add(
                with(new Input('text', 'os'))
                    ->setLabel('OS')
            );
            $this->add(
                with(new Input('text', 'osver'))
                    ->setLabel('OS Version')
            );
            $this->add(
                with(new Input('text', 'copy'))
                    ->setLabel('remote copy')
                    ->setRequired()
            );
            $this->add(
                with(new Input('text', 'shell'))
                    ->setLabel('shell cmd')
                    ->setRequired()
            );
            $this->add(
                with(new Input('text', 'path'))
                    ->setLabel('exec. path')
            );
            $this->add(
                with(new Input('text', 'codepath'))
                    ->setLabel('path to code')
            );
            $this->add(
                with(new Input('text', 'homepath'))
                    ->setLabel('path to home')
            );
            $this->add(
                with(new Input('text', 'backuppath'))
                    ->setLabel('path for backup')
                    ->setRequired()
            );
            $this->add(
                with(new Input('text', 'lockpath'))
                    ->setLabel('path for lockfile')
                    ->setRequired()
            );
            $this->add(
                with(new Input('text', 'scratchpath'))
                    ->setLabel('path for scratch')
                    ->setRequired()
            );
            $this->add(
                with(new Input('text', 'csql'))
                    ->setLabel('csql')
            );
            $this->add(
                with(new Input('text', 'bin'))
                    ->setLabel('path to bin')
                    ->setRequired()
            );
            $this->add(
                with(new Input('text', 'ldap'))
                    ->setLabel('path to ldap utils')
                    ->setRequired()
            );
            $this->add(
                with(new Input('text', 'sendmail'))
                    ->setLabel('sendmail command')
                    ->setRequired()
            );
            $this->add(
                with(new Input('text', 'publicKey'))
                    ->setLabel('public Key')
            );
            $this->add(
                with(new Input('text', 'sslcertf'))
                    ->setLabel('RS SSL Cert. (crt) filename')
            );
            $this->add(
                with(new Input('text', 'sslkeyf'))
                    ->setLabel('RS SSL key (key) filename')
            );
            $this->add(
                with(new Input('text', 'sslproxyipv4'))
                    ->setLabel('SSL Proxy Gateway IPv4')
            );
            $this->add(
                with(new Input('text', 'mysqlversion'))
                    ->setLabel('Mysql Version')
            );
            $this->add(
                with(new Input('text', 'apacheversion'))
                    ->setLabel('Apache version')
            );
            $this->add(
                with(new Input('text', 'apachegrp'))
                    ->setLabel('Apache group')
                    ->setRequired()
            );
            $this->add(
                with(new Input('text', 'dbadminuser'))
                    ->setLabel('DB Admin user')
            );
            $this->add(
                with(new Input('text', 'dbadminuserpw'))
                    ->setLabel('DB Admin user')
            );
        }

        $this->add(
            with(new TextArea('notes'))
                ->setLabel('Notes')
                ->setRows(4)
        );
    }

    public function rules(): array
    {
        return [
            'boxname' => [
                'required',
                'string',
                Rule::unique('box')->ignore($this->model->boxid, 'boxid')
            ],

            'lan' => 'required|exists:lan,lanid',
            'fqdn' => [
                'required',
                Rule::unique('box')->ignore($this->model->boxid, 'boxid')
            ]
        ];
    }
}