<?php

namespace App\Http\Formlets\Box;

use App\Box;
use App\Service;
use App\BoxService;
use RS\Form\Formlet;
use RS\Form\Fields\Input;
use RS\Form\Fields\Select;
use RS\Form\Fields\Checkbox;
use RS\Form\Fields\Hidden;

class BoxServiceCreateForm extends Formlet
{

    public $formView = "entities.box.forms.service";
    public $box;

    public function __construct(BoxService $boxService, Box $box)
    {
        $this->setModel($boxService);
        $this->box = $box;
    }

    public function prepareForm(): void
    {
        $this->add(
            with(new Hidden('boxid'))
                ->setValue($this->box->boxid)
                ->setRequired()
        );

        $this->add(
            with(new Select('srvid', $this->box->serviceOptions()))
                ->setLabel('Service')
                ->setRequired()
        );

        $this->add(
            with(new Input('text', 'uname'))
                ->setLabel('Service Username')
        );

        $this->add(
            with(new Input('text', 'ugroup'))
                ->setLabel('Service User Group')
        );

        $this->add(
            with(new Input('text', 'upword'))
                ->setLabel('Service User Password')
        );

        $this->add(
            with(new Input('text', 'path'))
                ->setLabel('Service Path')
        );

        $this->add(
            with(new Input('text', 'fqdn'))
                ->setLabel('FQDN')
        );

        $this->add(
            with(new Checkbox('remote', 'on', ''))
                ->setLabel('Usable from other boxes')
        );

        $this->add(
            with(new Checkbox('active', 'on', ''))
                ->setLabel('Active')
                ->setDefault(true)
        );

    }

}
