<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /** @var string  */
    protected $table = 'service';
    /** @var string  */
    protected $primaryKey = 'srvid';
    /** @var array  */
    protected $guarded = [];
    /** @var array  */
    protected $casts = [
        'active' => 'boolean'
    ];

    /**
     * Get class string active status
     * @return string
     */
    public function activeClass(): string
    {
        return $this->active ? 'active' : 'inactive';
    }

}
