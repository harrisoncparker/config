<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    /** @var string */
    protected $table = 'box';
    /** @var string */
    protected $primaryKey = 'boxid';
    /** @var array */
    protected $guarded = [];
    /** @var array */
    protected $casts = [
        'active' => 'boolean'
    ];

    public function activeClass()
    {
        return $this->active ? 'active' : 'inactive';
    }

    public static function options()
    {
        return with(new static)->orderBy('boxname')->pluck('boxname', 'boxid');
    }

    public static function clusterOptions()
    {
        return [
            'Boxes', 'With',
            'cluster', 'Service',
            'Set', 'To', 'Live'
        ];
    }

    /**
     * @return mixed
     */
    public function getActions()
    {
        return Action::where('class', $this->table)->get();
    }

    /**
     *  LAN Eloquent Relationship
     *
     * Notes: using 'boxLan' instead of lan to be able to use eager loading while not
     * changing the foreign key to 'lan_id' which would be ideal in the future.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function boxLan()
    {
        return $this->belongsTo('App\Lan', 'lan');
    }

    public function boxServices()
    {
        return $this->hasMany(BoxService::class, 'boxid');
    }

    public function serviceOptions()
    {
        return Service::whereNotIn('srvid', $this->boxServices()->pluck('srvid'))
            ->get()
            ->mapWithKeys(function ($service) {
                return [$service->srvid => $service->srvname];
            });
    }

}