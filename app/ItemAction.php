<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemAction extends Model
{
    /** @var string  */
    protected $table = 'itemactions';
    /** @var array  */
    protected $fillable = [
        'aid',
        'rid',
        'class',
        'status',
        'message'
    ];
    /** @var bool  */
    public $timestamps = false;

    /**
     * @return string
     */
    public function statusClass()
    {
        return $this->status ? 'success' : 'fail';
    }

}
