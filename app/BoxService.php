<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoxService extends Model
{

    /** @var string  */
    protected $table = 'boxservices';
    /** @var string  */
    protected $primaryKey = 'bsid';
    /** @var array */
    protected $guarded = [];
    /** @var array  */
    protected $casts = [
        'active' => 'boolean'
    ];

}
