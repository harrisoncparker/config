@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{route('services.create')}}">
        Create new Service
    </a>
@overwrite

@section('content')
    @include('entities.service.partials.table')
@overwrite