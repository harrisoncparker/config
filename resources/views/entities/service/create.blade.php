@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{route('services.index')}}">
        All Services
    </a>
@overwrite

@section('content')
    @nview('entities.service.forms.create')
@endsection