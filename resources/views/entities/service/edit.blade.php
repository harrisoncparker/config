@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{route('services.index')}}">
        All Services
    </a>
    @if(!$service->active)
        @include('entities.service.partials.delete')
    @endif
@overwrite

@section('content')
    @nview('entities.service.forms.edit')
    {{--<h3>Boxes delivering this service</h3>--}}
    {{--@include('entities.box.partials.table', ['boxes' => $service->boxes])--}}
@endsection

