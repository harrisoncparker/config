<table class="table">
    <tr>
        <th>Service</th>
        <th>Name</th>
        <th>Active</th>
        <th></th>
    </tr>
    @foreach($services as $service)
        <tr class="table__row table__row-{{$service->activeClass()}}">
            <td>{{ $service->srvid }}</td>
            <td>{{ $service->srvname }}</td>
            <td>{{ $service->active }}</td>
            <td class="text-right">
                <a class="btn btn-info" href="{{route('services.edit', ['service'=> $service])}}">
                    edit
                </a>
            </td>
        </tr>
    @endforeach
</table>