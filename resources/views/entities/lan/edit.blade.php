@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{route('lans.index')}}">
        All LANs
    </a>
    @if(!$lan->active)
        @include('entities.lan.partials.delete')
    @endif
    @foreach($lan->getActions() as $action)
        <a class="btn btn-info" href="{{route('lans.actions.show', ['lan'=> $lan, 'action' => $action])}}">
            {{$action->name}}
        </a>
    @endforeach
@overwrite

@section('content')
    @nview('entities.lan.forms.edit')
    <h3>Boxes on this LAN</h3>
    @include('entities.box.partials.table', ['boxes' => $lan->boxes])
@endsection

