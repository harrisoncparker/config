@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{route('lans.create')}}">
        Create new Lan
    </a>
@overwrite

@section('content')
    @include('entities.lan.partials.table')
@overwrite

