@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{route('lans.index')}}">
        All LANs
    </a>
@overwrite

@section('content')
        @nview('entities.lan.forms.create')
@endsection

