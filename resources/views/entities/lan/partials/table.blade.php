<table class="table">
    <tr>
        <th>LAN</th>
        <th>Name</th>
        <th>Boxes</th>
        <th>External Gateway</th>
        <th>Internal Gateway</th>
        <th>ip4</th>
        <th>ip4mask</th>
        <th>Active</th>
        <th>Access to Services</th>
        <th></th>
    </tr>
    @foreach($lans as $lan)
        <tr class="table__row table__row-{{$lan->activeClass()}}">
            <td>{{ $lan->lanid }}</td>
            <td>{{ $lan->lanname }}</td>
            <td>{{ $lan->boxesCount() }}</td>
            <td>{{ $lan->extgw }}</td>
            <td>{{ $lan->intgw }}</td>
            <td>{{ $lan->ip4 }}</td>
            <td>{{ $lan->ip4mask }}</td>
            <td>{{ $lan->active }}</td>
            <td>{{ $lan->access }}</td>
            <td class="text-right">
                <a class="btn btn-info" href="{{route('lans.edit', ['lan'=> $lan])}}">
                    Edit
                </a>
            </td>
        </tr>
    @endforeach
</table>