@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{route('boxes.edit', ['box' => $box])}}">
        Edit: {{$box->boxname}}
    </a>
@overwrite

@section('content')
    @foreach($boxServiceEditForms as $serviceName => $boxServiceEditForm)
        <h3>{{$serviceName}}</h3>
        {!! $boxServiceEditForm->render() !!}
    @endforeach
    @if($box->serviceOptions()->count() > 0)
        <h3>Add service to {{$box->boxname}}</h3>
        {!! $boxServiceCreateForm !!}
    @endif
@endsection