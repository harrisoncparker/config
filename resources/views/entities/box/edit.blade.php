@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{route('boxes.index')}}">
        All Boxes
    </a>
    <a class="btn btn-info" href="{{route('boxes.services.index', ['box' => $box])}}">
        Box Services
    </a>
    @if(!$box->active)
        @include('entities.box.partials.delete')
    @endif
    @foreach($box->getActions() as $action)
        <a class="btn btn-info" href="{{route('boxes.actions.show', ['box'=> $box, 'action' => $action])}}">
            {{$action->name}}
        </a>
    @endforeach
@overwrite

@section('content')
        @nview('entities.lan.forms.edit')
@endsection

