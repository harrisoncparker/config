<table class="table">
    <tr>
        <th>Box</th>
        <th>Name</th>
        <th>Lan</th>
        <th>FQDN</th>
        <th>IPv4</th>
        <th>Sites Active</th>
        <th>Sites Inactive</th>
        <th>Services</th>
        <th>Active</th>
        <th></th>
    </tr>
    @foreach($boxes as $box)
        <tr class="table__row table__row-{{$box->activeClass()}}">
            <td>{{ $box->boxid }}</td>
            <td>{{ $box->boxname }}</td>
            <td>
                <a href="{{route('lans.edit', ['lan'=> $box->boxLan]) }}">
                    {{ $box->boxLan->lanname }}
                </a>
            </td>
            <td>{{ $box->fqdn }}</td>
            <td>{{ $box->ipv4 }}</td>
            <td>{{-- $box->sitesActive() --}}</td>
            <td>{{-- $box->sitesInactive() --}}</td>
            <td>{{-- $box->services() --}}</td>
            <td>{{ $box->active }}</td>
            <td class="text-right">
                <a class="btn btn-info" href="{{route('boxes.edit', ['box'=> $box])}}">
                    Edit
                </a>
            </td>
        </tr>
    @endforeach
</table>