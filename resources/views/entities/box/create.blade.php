@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{route('boxes.index')}}">
        All Boxes
    </a>
@overwrite

@section('content')
    @nview('entities.box.forms.create')
@endsection

