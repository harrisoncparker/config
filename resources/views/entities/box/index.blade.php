@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{route('boxes.create')}}">
        Create new Box
    </a>
@overwrite

@section('content')
    @include('entities.box.partials.table')
@overwrite