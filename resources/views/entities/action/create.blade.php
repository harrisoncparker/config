@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{route('actions.index')}}">
        All Actions
    </a>
@overwrite

@section('content')
    @nview('entities.action.forms.create')
@endsection

