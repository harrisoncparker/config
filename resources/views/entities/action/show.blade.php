@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{$backButton['route']}}">
        {{$backButton['text']}}
    </a>
@overwrite

@section('content')
    <p>{{$command}}</p>
    <p>{{$description}}</p>
    <form action=""></form>
    <form action="{{$executeButton['route']}}" method="POST">
        <input name="_token" type="hidden" value="{{csrf_token()}}"/>
        <button name="_method" class="btn btn-info" value="post">Do Action</button>
    </form>
    <hr>
    <h2>Action History</h2>
    <table class="table">
        <tr>
            <th>Event</th>
            <th>Status</th>
            <th>Message</th>
        </tr>
        @foreach($history as $itemAction)
            <tr class="table__item table__item-{{$itemAction->statusClass()}}">
                <td>{{$itemAction->event}}</td>
                <td>{{$itemAction->status}}</td>
                <td>{{$itemAction->message}}</td>
            </tr>
        @endforeach
    </table>
@overwrite