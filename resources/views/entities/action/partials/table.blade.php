<table class="table">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Class</th>
        <th>Scope</th>
        <th>Sig</th>
        <th>Seq</th>
        <th>Active</th>
        <th></th>
    </tr>
    @foreach($actions as $action)
        <tr class="table__row table__row-{{$action->activeClass()}}">
            <td>{{ $action->id }}</td>
            <td>{{ $action->name }}</td>
            <td>{{ $action->class }}</td>
            <td>{{ $action->scope }}</td>
            <td>{{ $action->sig }}</td>
            <td>{{ $action->seq }}</td>
            <td>{{ $action->active }}</td>
            <td class="text-right">
                <a class="btn btn-info" href="{{route('actions.edit', ['action'=> $action])}}">
                    edit
                </a>
            </td>
        </tr>
    @endforeach
</table>