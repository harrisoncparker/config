@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{route('actions.create')}}">
        Create new Action
    </a>
@overwrite

@section('content')
    @include('entities.action.partials.table')
@overwrite

