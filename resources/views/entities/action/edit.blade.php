@extends('layouts.main')

@section('actions')
    <a class="btn btn-info" href="{{route('actions.index')}}">
        All Actions
    </a>
    @if(!$action->active)
        @include('entities.action.partials.delete')
    @endif
@overwrite

@section('content')
    @nview('entities.action.forms.edit')
@endsection

