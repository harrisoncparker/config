<header class="header">

    <div class="container">
        <div class="row">
            <a href="{{route('home')}}">
                <h1 class="header__logo col-md-4">CONFIG 2018</h1>
            </a>

            <div class="header__nav col-md-8 text-right">
                <a href="{{route('sites.index')}}" class="nav-menu__link btn btn-success">Sites</a>
                <a href="{{route('boxes.index')}}" class="nav-menu__link btn btn-success">Boxes</a>
                <a href="{{route('lans.index')}}" class="nav-menu__link btn btn-success">Lans</a>

                <a href="{{route('services.index')}}" class="nav-menu__link btn btn-info">Services</a>
                <a href="{{route('actions.index')}}" class="nav-menu__link btn btn-info">Actions</a>
                <a href="{{route('modules.index')}}" class="nav-menu__link btn btn-info">Modules</a>
            </div>
        </div>
    </div>

</header>