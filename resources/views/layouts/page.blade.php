<!DOCTYPE html>
<html lang="{{config('app.locale')}}" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="csrf-token" content="{{csrf_token()}}"/>
    <title>
        @hasSection('title')
            Config | @yield('title')
        @else
            Config | {{$title or 'RS Config'}}
        @endif
    </title>
    @include('components.analytics')
    <link rel="stylesheet" href="{{mix('css/app.css')}}"/>
</head>
<body>

@yield('content')

{{--<script src="{{mix('js/vendor.js')}}"></script>--}}
<script src="{{mix('js/app.js')}}"></script>
</body>
</html>
