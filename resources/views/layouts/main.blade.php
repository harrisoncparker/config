@extends('layouts.page')

@section('content')

    @include('components.header')

    <div class="container">
        <div class="row">

            <div class="sidebar col-md-2">
                @yield('actions')
            </div>

            <div class="main-content-wrapper col-md-10">
                @include('flash::message')
                @if(isset($title))
                    <h2>{{$title}}</h2>
                @endif
                @yield('content')
            </div>

        </div>
    </div>

    @include('components.footer')

@overwrite