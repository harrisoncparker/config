<?php

namespace Tests\Unit;

use App\Lan;
use Tests\TestCase;

class LanTest extends TestCase
{

    /** @test */
    public function lan_active_is_transformed_to_bool()
    {
        $lan = factory(Lan::class)->make([
            'active' => 'on'
        ]);
        // Assertions
        $this->assertInternalType(
            'boolean',
            $lan->active,
            'Lan active did not return as a boolean.'
        );
        $this->assertTrue(
            $lan->active,
            'Lan active "on" did not return as true.'
        );
        $this->assertNotEquals(
            $lan->active,
            'on'
        );
    }

    /** @test */
    public function lan_access_is_transformed_to_bool()
    {
        $lan = factory(Lan::class)->make([
            'access' => 'on'
        ]);
        // Assertions
        $this->assertInternalType(
            'boolean',
            $lan->access,
            'Lan access did not return as a boolean.'
        );
        $this->assertTrue(
            $lan->access,
            'Lan access did not return as true.'
        );
        $this->assertNotEquals(
            $lan->access,
            'on'
        );
    }

}