<?php

namespace Tests\Unit;

use App\Action;
use Tests\TestCase;

class ActionTest extends TestCase
{

    /** @test */
    public function can_get_action_class_values()
    {
        // create an action
        $action = Factory(Action::class)->make();
        $classValues = $action->getClassValues();
        // Insure an array was returned
        $this->assertInternalType(
            'array',
            $classValues,
            '$action->getClassValues() did not return an array.'
        );
        // insure that the array is not empty
        $this->assertNotEmpty(
            $classValues,
            '$action->getClassValues() returned and empty array.'
        );
    }

    /** @test */
    public function can_get_action_class_options()
    {
        // create an action
        $action = Factory(Action::class)->make();
        $classOptions = $action->getClassOptions();
        // Insure an object was returned
        $this->assertInternalType(
            'object',
            $classOptions,
            '$action->getClassOptions() did not return an object.'
        );
        // insure that the object is not empty
        $this->assertNotEmpty(
            $classOptions,
            '$action->getClassOptions() returned and empty object.'
        );
    }

    /** @test */
    public function can_get_action_scope_values()
    {
        // create an action
        $action = Factory(Action::class)->make();
        $scopeValues = $action->getScopeValues();
        // Insure an array was returned
        $this->assertInternalType(
            'array',
            $scopeValues,
            '$action->getScopeValues() did not return an array.'
        );
        // insure that the array is not empty
        $this->assertNotEmpty(
            $scopeValues,
            '$action->getScopeValues() returned and empty array.'
        );
    }

    /** @test */
    public function can_get_action_scope_options()
    {
        // create an action
        $action = Factory(Action::class)->make();
        $scopeOptions = $action->getScopeOptions();
        // Insure an object was returned
        $this->assertInternalType(
            'object',
            $scopeOptions,
            '$action->getScopeOptions() did not return an object.'
        );
        // insure that the object is not empty
        $this->assertNotEmpty(
            $scopeOptions,
            '$action->getScopeOptions() returned and empty object.'
        );
    }

}