<?php

namespace Tests\Feature;

use App\Box;
use App\Service;
use App\BoxService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BoxServiceTest extends TestCase
{

    use RefreshDatabase;

    protected function validParams($overrides = []): array
    {
        return array_merge([
            'boxid' => 1,
            'srvid' => 1,
            'active' => 'on'
        ], $overrides);
    }

    /** @test */
    public function can_view_all_boxservice_edit_forms_related_to_a_box()
    {
        $this->withoutExceptionHandling();
        // create Box
        $theBox = factory(Box::class)->create(['boxname' => 'theBox']);
        // create Services
        factory(Service::class)->create(['srvname' => 'firstService']);
        factory(Service::class)->create(['srvname' => 'secondService']);
        // create a box service for each box with testable data
        BoxService::create($this->validParams([
            'ugroup' => 'aTestUserGroup',
            'path' => 'a/test/path'
        ]));
        BoxService::create($this->validParams([
            'srvid' => 2,
            'uname' => 'aTestUserName',
            'path' => 'another/test/path'
        ]));
        // get the BoxService view for this box
        $response = $this->get(route('boxes.services.index', ['box' => $theBox->boxid]));
        $response->assertStatus(200);
        // make sure expected data is displayed in response
        $response->assertSee('<form');
        // box service 1
        $response->assertSee('firstService');
        $response->assertSee('aTestUserGroup');
        $response->assertSee('a/test/path');
        // box service 2
        $response->assertSee('secondService');
        $response->assertSee('aTestUserName');
        $response->assertSee('another/test/path');
    }

    /** @test */
    public function can_view_create_boxservice_form()
    {
        $this->withoutExceptionHandling();
        // create Box
        $theBox = factory(Box::class)->create(['boxname' => 'theBox']);
        // create Services
        factory(Service::class)->create(['srvname' => 'firstService']);
        factory(Service::class)->create(['srvname' => 'secondService']);
        // get the BoxService view for this box
        $response = $this->get(route('boxes.services.index', ['box' => $theBox->boxid]));
        $response->assertStatus(200);
        // make sure expected data is displayed in response
        $response->assertSee('<form');
    }

    /** @test */
    public function can_create_boxservice()
    {
        $this->withoutExceptionHandling();
        // create Box
        factory(Box::class)->create(['boxname' => 'theBox']);
        // create Services
        factory(Service::class)->create(['srvname' => 'firstService']);
        // post to create box service
        $this->post(route('boxservices.store'), $this->validParams([
            'uname' => 'aTestUserName',
            'ugroup' => 'aTestUserGroup',
            'path' => 'a/test/path'
        ]))->assertStatus(302);
        // get the first box service
        if($boxService = BoxService::first()){
            $this->assertEquals($boxService->boxid, 1);
            $this->assertEquals($boxService->srvid, 1);
            $this->assertEquals($boxService->uname, 'aTestUserName');
            $this->assertEquals($boxService->ugroup, 'aTestUserGroup');
            $this->assertEquals($boxService->path, 'a/test/path');
        } else {
            $this->fail('Box Service was not created');
        }
    }

}