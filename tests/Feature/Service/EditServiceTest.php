<?php

namespace Tests\Feature;

use App\Service;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EditServiceTest extends TestCase
{

    use RefreshDatabase;

    protected function validParams($overrides = []): array
    {
        // Default valid values
        return array_merge([
            'srvname' => 'TestService',
            'active' => 'on'
        ], $overrides);
    }

    private function prepareDatabaseForTesting($newParams = [])
    {
        $service = Service::create($this->validParams());
        return $this->patch(route('services.update', $service), $newParams);
    }

    /** @test */
    public function should_be_able_to_update_a_service()
    {
        // Create and Edit Service
        $this->prepareDatabaseForTesting([
            'srvname' => 'NewTestName',
            'active' => ''
        ]);
        // Get Service
        if ($service = Service::first()) {
            // Test Service values
            $this->assertEquals($service->srvname, "NewTestName");
            $this->assertFalse($service->active);
        } else {
            $this->fail('Service was not created');
        }
    }

    /** @test */
    public function srvname_is_required()
    {
        // Create Service and test for srvname errors
        $this->prepareDatabaseForTesting(['srvname' => ''])
            ->assertSessionHasErrors('srvname');
    }

    /** @test */
    public function srvname_must_be_unique()
    {
        // Create Service
        $this->prepareDatabaseForTesting(['srvname'=>'notunique'])
            ->assertStatus(302);

        $service = Service::first();

        $this->patch(route('services.update', $service), [
            'srvid'=> $service->srvid,
            'srvname'=>'notunique'
        ])->assertStatus(302);

    }


}