<?php

namespace Tests\Feature;

use App\Service;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteServiceTest extends TestCase
{

    use RefreshDatabase;

    private function createAndDeleteService($active = '')
    {
        // Create and inactive Service
        $service = factory(Service::class)->create([
            'active' => $active
        ]);
        // check to see if Service exists
        $this->assertDatabaseHas($service->getTable(),$service->getAttributes());
        // Attempt to delete inactive Service
        return [
            'res' => $this->delete("/services/{$service->srvid}"),
            'service' => $service
        ];
    }

    /** @test */
    public function can_delete_inactive_service()
    {
        // Attempt to delete inactive service
        $result = $this->createAndDeleteService();
        // Assert response redirect
        $result['res']->assertRedirect('/services');
        // Check to see if service has been deleted
        $this->assertDatabaseMissing(
            $result['service']->getTable(),
            $result['service']->getAttributes()
        );
    }

    /** @test */
    public function cant_delete_active_service()
    {
        // Attempt to delete inactive service
        $result = $this->createAndDeleteService('on');
        // Assert response redirect
        $result['res']->assertRedirect("/services/{$result['service']->srvid}/edit");
        // Check to see if service remains in database
        $this->assertDatabaseHas(
            $result['service']->getTable(),
            $result['service']->getAttributes()
        );
    }

}