<?php

namespace Tests\Feature;

use App\Service;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateServiceTest extends TestCase
{

    use RefreshDatabase;

    protected function validParams($overrides = []): array
    {
        // Default valid values
        return array_merge([
            'srvname' => 'TestService',
            'active' => 'on'
        ], $overrides);
    }

    private function prepareDatabaseForTesting($overrides = [])
    {
        return $this->post(route('services.store'), $this->validParams($overrides));
    }

    /** @test */
    public function should_be_able_to_create_a_service()
    {
        // Create Service
        $this->prepareDatabaseForTesting();
        // Get Service
        if ($service = Service::first()) {
            // Test Service values
            $this->assertEquals($service->srvname, "TestService");
            $this->assertTrue($service->active);
        } else {
            $this->fail('Service was not created');
        }
    }

    /** @test */
    public function srvname_is_required()
    {
        // Create Service and test for srvname errors
        $this->prepareDatabaseForTesting(['srvname' => ''])
            ->assertSessionHasErrors('srvname');
    }

    /** @test */
    public function srvname_must_be_unique()
    {
        // Create Service
        $this->prepareDatabaseForTesting(['srvname'=>'notunique'])
            ->assertStatus(302);
        $this->prepareDatabaseForTesting(['srvname'=>'notunique'])
            ->assertSessionHasErrors('srvname');
    }

}