<?php

namespace Tests\Feature;

use App\Service;
use App\Box;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewServicesTest extends TestCase
{

    use RefreshDatabase;

    protected function validParams($overrides=[]): array
    {
        return array_merge([
            'srvname' => 'TestService',
            'active' => 'on'
        ],$overrides);
    }

    /** @test */
    public function can_view_list_of_services()
    {
        $this->withoutExceptionHandling();
        // Create two valid services
        Service::create($this->validParams());
        Service::create($this->validParams([
            'srvname' => 'AnotherService',
            'active' => ''
        ]));
        // Get Service index page
        $response = $this->get(route('services.index'));
        // Assert response status
        $response->assertStatus(200);
        // Check to see if values are displayed
        $response->assertSee('TestService');
        // Second Service
        $response->assertSee('AnotherService');
    }

    /** @test */
    function user_can_view_service_create_form()
    {
        $this->withoutExceptionHandling();
        $response = $this->get(route('services.create'));
        $response->assertStatus(200, 'View create form failed');
        $response->assertSee('Create Service form');
        $response->assertSee('<form');
    }

    /** @test */
    function user_can_view_service_edit_form()
    {
        $this->withoutExceptionHandling();
        $service1 = Service::create($this->validParams());
        $response = $this->get(route('services.edit',[$service1]));
        $response->assertStatus(200, 'View edit form failed');
        $response->assertSee('Edit Service: '. $service1->srvname);
        $response->assertSee('<form');
    }

}