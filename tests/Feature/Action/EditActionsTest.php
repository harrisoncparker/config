<?php

namespace Tests\Feature;

use App\Action;

class EditActionsTest extends CreateActionsTest
{
    private function prepareForValidation($newParams = [])
    {
        $action = Action::create($this->validParams());
        return $this->patch(route('actions.update', $action), $newParams);
    }

    /** @test */
    public function should_be_able_to_update_a_action()
    {
        // Create and Edit Action
        $this->prepareForValidation([
            'name' => 'NewActionName',
            'class' => 'site',
            'scope' => 'init',
            'sig' => 'ba_test22',
            'command' => 'reins box test',
            'active' => '',
            'notes' => 'Lorum Ipsum ... foo bar',
        ]);

        $editedAction = Action::first();

        // Test ACTION values have changed
        $this->assertEquals($editedAction->name, "NewActionName");
        $this->assertEquals($editedAction->class, "site");
        $this->assertEquals($editedAction->scope, "init");
        $this->assertEquals($editedAction->sig, "ba_test22");
        $this->assertEquals($editedAction->command, "reins box test");
        $this->assertFalse($editedAction->active);
        $this->assertEquals($editedAction->notes, "Lorum Ipsum ... foo bar");
    }

}