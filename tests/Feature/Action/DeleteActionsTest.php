<?php

namespace Tests\Feature;

use App\Action;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteActionsTest extends TestCase
{
    use RefreshDatabase;

    private function createAndDeleteAction($active = '')
    {
        // Create and inactive action
        $action = factory(Action::class)->create([
            'active' => $active
        ]);
        // check to see if action exists
        $this->assertDatabaseHas($action->getTable(),$action->getAttributes());
        // Attempt to delete inactive action
        return [
            'res' => $this->delete("/actions/{$action->id}"),
            'action' => $action
        ];
    }

    /** @test */
    public function can_delete_inactive_action()
    {
        // Attempt to delete inactive action
        $result = $this->createAndDeleteAction();
        // Assert response redirect
        $result['res']->assertRedirect('/actions');
        // Check to see if action has been deleted
        $this->assertDatabaseMissing(
            $result['action']->getTable(),
            $result['action']->getAttributes()
        );
    }

    /** @test */
    public function cant_delete_active_action()
    {
        // Attempt to delete inactive lan
        $result = $this->createAndDeleteAction(true);
        // Assert response redirect
        $result['res']->assertRedirect("/actions/{$result['action']->id}/edit");
        // Check to see if lan remains in database
        $this->assertDatabaseHas(
            $result['action']->getTable(),
            $result['action']->getAttributes()
        );
    }

}