<?php

namespace Tests\Feature;

use App\Action;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateActionsTest extends TestCase
{

    use RefreshDatabase;

    protected function validParams($overrides = []): array
    {
        return array_merge([
            'name' => 'TestAction',
            'class' => 'box',
            'scope' => 'action',
            'sig' => 'ba_test1',
            'command' => 'reins box test test',
            'active' => 'on',
            'notes' => 'Lorum Ipsum ...'
        ], $overrides);
    }

    private function prepareDatabaseForTesting($overrides = [])
    {
        return $this->post(route('actions.store'), $this->validParams($overrides));
    }

    /** @test */
    public function should_be_able_to_create_an_action()
    {
        // Create Action
        $this->prepareDatabaseForTesting()->assertStatus(302);
        // Get Action
        if ($action = Action::first()) {
            // Test Action values
            $this->assertEquals($action->name, "TestAction");
            $this->assertEquals($action->class, "box");
            $this->assertEquals($action->scope, "action");
            $this->assertEquals($action->sig, "ba_test1");
            $this->assertEquals($action->command, "reins box test test");
            $this->assertTrue($action->active);
            $this->assertEquals($action->notes, "Lorum Ipsum ...");
        } else {
            $this->fail('Action was not created');
        }
    }

    /** @test */
    public function name_is_required()
    {
        $this->prepareDatabaseForTesting(['name' => ''])
            ->assertSessionHasErrors('name');
    }

    /** @test */
    public function name_must_be_unique()
    {
        Factory(Action::class)->create(['name'=>'notunique']);
        $this->prepareDatabaseForTesting(['name'=>'notunique'])
            ->assertSessionHasErrors('name');
    }

    /** @test */
    public function scope_is_required()
    {
        $this->prepareDatabaseForTesting(['scope' => ''])
            ->assertSessionHasErrors('scope');
    }

    /** @test */
    public function scope_is_one_of_defined_valid_scopes()
    {
        $this->prepareDatabaseForTesting(['scope' => 'Not a valid class'])
            ->assertSessionHasErrors('scope');
    }

    /** @test */
    public function class_is_required()
    {
        $this->prepareDatabaseForTesting(['class' => ''])
            ->assertSessionHasErrors('class');
    }

    /** @test */
    public function class_is_one_of_defined_valid_classes()
    {
        $this->prepareDatabaseForTesting(['class' => 'Not a valid class'])
            ->assertSessionHasErrors('class');
    }

    /** @test */
    public function sig_is_required()
    {
        $this->prepareDatabaseForTesting(['sig' => ''])
            ->assertSessionHasErrors('sig');
    }

    /** @test */
    public function sig_must_be_unique()
    {
        Factory(Action::class)->create(['sig'=>'notunique']);
        $this->prepareDatabaseForTesting(['sig'=>'notunique'])
            ->assertSessionHasErrors('sig');
    }

    /** @test */
    public function command_is_required()
    {
        $this->prepareDatabaseForTesting(['command' => ''])
            ->assertSessionHasErrors('command');
    }

    /** @test */
    public function command_must_be_unique()
    {
        Factory(Action::class)->create(['command'=>'notunique']);
        $this->prepareDatabaseForTesting(['command'=>'notunique'])
            ->assertSessionHasErrors('command');
    }

}