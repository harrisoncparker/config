<?php

namespace Tests\Feature;

use App\Action;
use App\Lan;
use Faker\Factory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewActionsTest extends TestCase
{

    use RefreshDatabase;

    protected function validParams($overrides=[]): array
    {
        return array_merge([
            'name' => 'TestAction',
            'class' => 'Box',
            'scope' => 'Action',
            'sig' => 'ba_test1',
            'command' => 'reins box test test',
            'active' => 'on',
            'notes' => 'Lorum Ipsum ...',
        ],$overrides);
    }

    /** @test */
    public function can_view_list_of_actions()
    {
        // Create two valid actions
        Action::create($this->validParams());
        Action::create($this->validParams([
            'name' => 'AnotherAction',
            'class' => 'Lan',
            'scope' => 'Maint',
            'sig' => 'lm_test2',
            'command' => 'reins lan test test test',
            'active' => '',
            'notes' => 'Dummy Text ...',
        ]));
        // Get Action index page
        $response = $this->get(route('actions.index'));
        // Assert response status
        $response->assertStatus(200);
        // Check to see if values are displayed
        $response->assertSee('TestAction');
        $response->assertSee('Box');
        $response->assertSee('Action');
        $response->assertSee('ba_test1');
        $response->assertSee('on');
        // Second Action
        $response->assertSee('AnotherAction');
        $response->assertSee('Lan');
        $response->assertSee('Maint');
        $response->assertSee('lm_test2');
    }

    /** @test */
    function user_can_view_action_create_form()
    {
        $response = $this->get(route('actions.create'));
        $response->assertStatus(200, 'View create form failed');
        $response->assertSee('Create Action form');
        $response->assertSee('<form');
    }

    /** @test */
    function user_can_view_action_edit_form()
    {
        $action1 = Action::create($this->validParams());
        $response = $this->get(route('actions.edit',[$action1]));
        $response->assertStatus(200, 'View edit form failed');
        $response->assertSee('Edit Action: '. $action1->name);
        $response->assertSee('<form');
    }

}