<?php

namespace Tests\Feature;

use App\Lan;
use App\Box;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteLansTest extends TestCase
{

    use RefreshDatabase;

    private function createAndDeleteLan($active = '')
    {
        // Create and inactive lan
        $lan = factory(Lan::class)->create([
            'active' => $active
        ]);
        // check to see if lan exists
        $this->assertDatabaseHas($lan->getTable(),$lan->getAttributes());
        // Attempt to delete inactive lan
        return [
            'res' => $this->delete("/lans/{$lan->lanid}"),
            'lan' => $lan
        ];
    }

    /** @test */
    public function can_delete_inactive_lan()
    {
        // Attempt to delete inactive lan
        $result = $this->createAndDeleteLan();
        // Assert response redirect
        $result['res']->assertRedirect('/lans');
        // Check to see if lan has been deleted
        $this->assertDatabaseMissing(
            $result['lan']->getTable(),
            $result['lan']->getAttributes()
        );
    }

    /** @test */
    public function cant_delete_active_lan()
    {
        // Attempt to delete inactive lan
        $result = $this->createAndDeleteLan('on');
        // Assert response redirect
        $result['res']->assertRedirect("/lans/{$result['lan']->lanid}/edit");
        // Check to see if lan remains in database
        $this->assertDatabaseHas(
            $result['lan']->getTable(),
            $result['lan']->getAttributes()
        );
    }

    /** @test */
    public function cant_delete_lan_that_has_boxes()
    {
        // Attempt to delete inactive lan
        $lan = factory(Lan::class)->create([
            'active' => ''
        ]);
        factory(Box::class)->create([
            'lan' => $lan->lanid
        ]);
        // Try to delete lan
        $res = $this->delete("/lans/{$lan->lanid}");
        // Assert response redirect
        $res->assertRedirect("/lans/{$lan->lanid}/edit");
        // Check to see if lan remains in database
        $this->assertDatabaseHas(
            $lan->getTable(),
            $lan->getAttributes()
        );
    }

}