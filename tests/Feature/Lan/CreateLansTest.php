<?php

namespace Tests\Feature;

use App\Lan;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateLansTest extends TestCase
{

    use RefreshDatabase;

    protected function validParams($overrides = []): array
    {
        // Default valid values
        return array_merge([
            'lanname' => 'TestLan',
            'active' => 'on',
            'access' => '',
            'ip4' => '0.0.0.0',
            'ip4mask' => '0.0.0.0',
            'notes' => 'This is a test lan',
        ], $overrides);
    }

    private function prepareDatabaseForTesting($overrides = [])
    {
        return $this->post(route('lans.store'), $this->validParams($overrides));
    }

    /** @test */
    public function should_be_able_to_create_a_lan()
    {
        // Create LAN
        $this->prepareDatabaseForTesting();
        // Get LAN
        if ( $lan = Lan::first()) {
            // Test LAN values
            $this->assertEquals($lan->lanname, "TestLan");
            $this->assertTrue($lan->active);
            $this->assertFalse($lan->access);
            $this->assertEquals($lan->ip4, "0.0.0.0");
            $this->assertEquals($lan->ip4mask, "0.0.0.0");
            $this->assertEquals($lan->notes, "This is a test lan");
        } else {
            $this->fail('LAN was not created');
        }
    }

    /** @test */
    public function lanname_is_required()
    {
        // Create LAN and test for lanname errors
        $this->prepareDatabaseForTesting(['lanname' => ''])
            ->assertSessionHasErrors('lanname');
    }

    /** @test */
    public function lanname_must_be_unique()
    {
        Factory(Lan::class)->create(['lanname'=>'notunique']);
        $this->prepareDatabaseForTesting(['lanname'=>'notunique'])
            ->assertSessionHasErrors('lanname');
    }

    /** @test */
    public function ip4_is_required()
    {
        // Create LAN and test for lanname errors
        $this->prepareDatabaseForTesting(['ip4' => ''])
            ->assertSessionHasErrors('ip4');
    }

    /** @test */
    public function ip4_must_be_valid_ip4_address()
    {
        // Create LAN and test for lanname errors
        $this->prepareDatabaseForTesting(['ip4' => 'invalid_ip_address'])
            ->assertSessionHasErrors('ip4');
    }

    /** @test */
    public function ip4mask_is_not_required()
    {
        // Create LAN and test for lanname errors
        $this->prepareDatabaseForTesting(['ip4mask' => ''])
            ->assertSessionHasErrors('ip4mask');
    }

    /** @test */
    public function ip4mask_must_be_valid_ip4_address()
    {
        // Create LAN and test for lanname errors
        $this->prepareDatabaseForTesting(['ip4mask' => 'invalid_ip_address'])
            ->assertSessionHasErrors('ip4mask');
    }

}