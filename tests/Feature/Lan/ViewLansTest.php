<?php

namespace Tests\Feature;

use App\Lan;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewLansTest extends TestCase
{

    use RefreshDatabase;

    protected function validParams($overrides=[]): array
    {
        return array_merge([
            'lanname' => 'TestLan',
            'active' => 'on',
            'access' => '',
            'ip4' => '0.0.0.0',
            'ip4mask' => '0.0.0.0',
            'notes' => 'This is a test lan',
        ],$overrides);
    }

    protected function updateProfile($params = [])
    {
        Storage::fake('local');
        return $this->put("/members/profile", array_merge($this->validParams(), $params));
    }

    /** @test */
    function user_can_view_list_of_lans()
    {
        // Create two Lans with valid parameters
        Lan::create($this->validParams());
        Lan::create($this->validParams([
            'lanname' => 'AmazonVPC',
            'access' => 'on',
            'ip4' => '10.0.0.0',
            'ip4mask' => '255.255.0.0',
            'notes' => 'The main VPC for Amazon',
        ]));
        // Get Lan index page
        $response = $this->get(route('lans.index'));
        // Assert response status
        $response->assertStatus(200);
        // Check to see if all Lans are displayed on the page
        $response->assertSee('AmazonVPC');
        $response->assertSee('on');
        $response->assertSee('10.0.0.0');
        $response->assertSee('255.255.0.0');
        $response->assertSee('TestLan');
        $response->assertSee('0.0.0.0');
    }

    /** @test */
    function user_can_view_lan_create_form()
    {
        $response = $this->get(route('lans.create'));
        $response->assertStatus(200, 'View create form failed');
        $response->assertSee('Create LAN');
        $response->assertSee('<form');
    }

    /** @test */
    function user_can_view_lan_edit_form()
    {
        $lan1 = Lan::create($this->validParams());
        $response = $this->get(route('lans.edit',[$lan1]));
        $response->assertStatus(200, 'View edit form failed');
        $response->assertSee('Edit LAN: '. $lan1->lanname);
        $response->assertSee('<form');
    }


}