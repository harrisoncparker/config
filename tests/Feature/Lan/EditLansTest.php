<?php

namespace Tests\Feature;

use App\Lan;

class EditLansTest extends CreateLansTest
{

    private function prepareForValidation($newParams = [])
    {
        $lan = Lan::create($this->validParams());
        return $this->patch(route('lans.update', $lan), $newParams);
    }

    /** @test */
    public function should_be_able_to_update_a_lan()
    {
        // Create and Edit LAN
        $this->prepareForValidation([
            'lanname' => 'NewTestName',
            'active' => '',
            'access' => 'on',
            'extgw' => '0.0.0.0',
            'intgw' => '0.0.0.0',
            'ip4' => '10.11.12.13',
            'ip4mask' => '10.0.0.0',
            'notes' => 'Some updated notes'
        ]);

        $editedLan = Lan::first();

        // Test LAN values have changed
        $this->assertEquals($editedLan->lanname, "NewTestName");
        $this->assertFalse($editedLan->active);
        $this->assertTrue($editedLan->access);
        $this->assertEquals($editedLan->extgw, "0.0.0.0");
        $this->assertEquals($editedLan->intgw, "0.0.0.0");
        $this->assertEquals($editedLan->ip4, "10.11.12.13");
        $this->assertEquals($editedLan->ip4mask, "10.0.0.0");
        $this->assertEquals($editedLan->notes, "Some updated notes");
    }


}