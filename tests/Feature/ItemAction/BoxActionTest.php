<?php

namespace Tests\Feature;

use App\ItemAction;
use App\Box;
use App\Action;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BoxActionTest extends TestCase
{
    use RefreshDatabase;

    private function createBox($overrides = [])
    {
        return Factory(Box::class)->create(array_merge([
            'boxname' => 'My Test Box'
        ], $overrides));
    }

    private function createActionForBox($overrides = [])
    {
        return Factory(Action::class)->create(array_merge([
            'name' => 'Test Action',
            'class' => 'box',
            'command' => 'reins box init action',
            'notes' => 'A description of the action'
        ], $overrides));
    }

    private function executeBoxAction()
    {
        // create box and action
        $this->createBox();
        $this->createActionForBox();
        // get box and action
        $box = Box::first();
        $action = Action::first();
        // execute box action
        return $this->post(route('boxes.actions.execute', [
            'box' => $box,
            'action' => $action
        ]));
    }

    /** @test */
    public function can_view_an_action_for_a_box()
    {
        // create box and action
        $this->createBox();
        $this->createActionForBox();
        // get box and action
        $box = Box::first();
        $action = Action::first();
        // get box action page
        $response = $this->get(route('boxes.actions.show', [
            'box' => $box,
            'action' => $action
        ]));
        $response->assertStatus(200);
        // Check response for expected strings
        $response->assertSee('Test Action for box My Test Box');
        $response->assertSee('reins box init action');
        $response->assertSee('A description of the action');
    }

    /** @test */
    public function cant_view_box_action_for_a_non_box_action()
    {
        // create box and action
        $this->createBox();
        $this->createActionForBox(['class' => 'site']);
        // get box and action
        $box = Box::first();
        $action = Action::first();
        // get box action page
        $response = $this->get(route('boxes.actions.show', [
            'box' => $box,
            'action' => $action
        ]));
        // assert 404
        $response->assertStatus(404);
    }

    /** @test */
    public function can_execute_box_action()
    {
        // execute box action
        $response = $this->executeBoxAction();
        // assert response
        $response->assertStatus(302);
    }

    /** @test */
    public function can_create_box_action()
    {
        // create ItemAction
        ItemAction::create([
            'aid' => 1,
            'rid' => 1,
            'class' => 'box',
            'status' => 1,
            'message' => 'This is the message produced by running the command'
        ]);
        // get ItemAction
        $itemaction = ItemAction::first();
        // Check if ItemAction exists
        $this->assertEquals($itemaction->aid, 1);
        $this->assertEquals($itemaction->rid, 1);
        $this->assertEquals($itemaction->class, 'box');
        $this->assertEquals($itemaction->status, 1);
        $this->assertEquals($itemaction->message, 'This is the message produced by running the command');
    }

    /** @test */
    public function box_action_is_stored_in_itemaction_table_when_executed()
    {
        // execute box action
        $response = $this->executeBoxAction();
        // assert response
        $response->assertStatus(302);
        // get ItemAction
        $itemaction = ItemAction::first();
        // Check if ItemAction exists
        $this->assertEquals($itemaction->aid, 1);
        $this->assertEquals($itemaction->rid, 1);
        $this->assertEquals($itemaction->class, 'box');
        $this->assertEquals($itemaction->status, 0);
        $this->assertEquals($itemaction->message, 'This is an example command message.');
    }
}