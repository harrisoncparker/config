<?php

namespace Tests\Feature;

use App\ItemAction;
use App\Lan;
use App\Action;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LanActionTest extends TestCase
{
    use RefreshDatabase;

    private function createLan($overrides = [])
    {
        return Factory(Lan::class)->create(array_merge([
            'lanname' => 'My Test LAN'
        ], $overrides));
    }

    private function createActionForLan($overrides = [])
    {
        return Factory(Action::class)->create(array_merge([
            'name' => 'Test Action',
            'class' => 'lan',
            'command' => 'reins lan init action',
            'notes' => 'A description of the action'
        ], $overrides));
    }

    private function executeLanAction()
    {
        // create lan and action
        $this->createLan();
        $this->createActionForLan();
        // get lan and action
        $lan = Lan::first();
        $action = Action::first();
        // execute lan action
        return $this->post(route('lans.actions.execute', [
            'lan' => $lan,
            'action' => $action
        ]));
    }

    /** @test */
    public function can_view_an_action_for_a_lan()
    {
        // create lan and action
        $this->createLan();
        $this->createActionForLan();
        // get lan and action
        $lan = Lan::first();
        $action = Action::first();
        // get lan action page
        $response = $this->get(route('lans.actions.show', [
            'lan' => $lan,
            'action' => $action
        ]));
        $response->assertStatus(200);
        // Check response for expected strings
        $response->assertSee('Test Action for lan My Test LAN');
        $response->assertSee('reins lan init action');
        $response->assertSee('A description of the action');
    }

    /** @test */
    public function cant_view_lan_action_for_a_non_lan_action()
    {
        // create lan and action
        $this->createLan();
        $this->createActionForLan(['class' => 'box']);
        // get lan and action
        $lan = Lan::first();
        $action = Action::first();
        // get lan action page
        $response = $this->get(route('lans.actions.show', [
            'lan' => $lan,
            'action' => $action
        ]));
        // assert 404
        $response->assertStatus(404);
    }

    /** @test */
    public function can_execute_lan_action()
    {
        // execute lan action
        $response = $this->executeLanAction();
        // assert response
        $response->assertStatus(302);
    }

    /** @test */
    public function can_create_lan_action()
    {
        // create ItemAction
        ItemAction::create([
            'aid' => 1,
            'rid' => 1,
            'class' => 'lan',
            'status' => 1,
            'message' => 'This is the message produced by running the command'
        ]);
        // get ItemAction
        $itemaction = ItemAction::first();
        // Check if ItemAction exists
        $this->assertEquals($itemaction->aid, 1);
        $this->assertEquals($itemaction->rid, 1);
        $this->assertEquals($itemaction->class, 'lan');
        $this->assertEquals($itemaction->status, 1);
        $this->assertEquals($itemaction->message, 'This is the message produced by running the command');
    }

    /** @test */
    public function lan_action_is_stored_in_itemaction_table_when_executed()
    {
        // execute lan action
        $response = $this->executeLanAction();
        // assert response
        $response->assertStatus(302);
        // get ItemAction
        $itemaction = ItemAction::first();
        // Check if ItemAction exists
        $this->assertEquals($itemaction->aid, 1);
        $this->assertEquals($itemaction->rid, 1);
        $this->assertEquals($itemaction->class, 'lan');
        $this->assertEquals($itemaction->status, 0);
        $this->assertEquals($itemaction->message, 'This is an example command message.');
    }
}