<?php

namespace Tests\Feature;

use App\Box;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteBoxTest extends TestCase
{
    use RefreshDatabase;

    private function createAndDeleteBox($active = '')
    {
        // Create and inactive box
        $box = factory(Box::class)->create([
            'active' => $active
        ]);
        // check to see if box exists
        $this->assertDatabaseHas($box->getTable(),$box->getAttributes());
        // Attempt to delete box
        return [
            'res' => $this->delete("/boxes/{$box->boxid}"),
            'box' => $box
        ];
    }

    /** @test */
    public function can_delete_inactive_box()
    {
        // Attempt to delete inactive box
        $result = $this->createAndDeleteBox();
        // Assert response redirect
        $result['res']->assertRedirect('/boxes');
        // Check to see if box has been deleted
        $this->assertDatabaseMissing(
            $result['box']->getTable(),
            $result['box']->getAttributes()
        );
    }

    /** @test */
    public function cant_delete_active_box()
    {
        // Attempt to delete inactive lan
        $result = $this->createAndDeleteBox('on');
        // Assert response redirect
        $result['res']->assertRedirect("/boxes/{$result['box']->boxid}/edit");
        // Check to see if lan remains in database
        $this->assertDatabaseHas(
            $result['box']->getTable(),
            $result['box']->getAttributes()
        );
    }

}