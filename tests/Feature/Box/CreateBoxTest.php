<?php

namespace Tests\Feature;

use App\Box;
use App\Lan;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateBoxTest extends TestCase
{

    use RefreshDatabase;

    protected function validParams($overrides = []): array
    {
        return array_merge([
            'boxname' => 'TestBox',
            'lan' => 1,
            'fqdn' => 'test-box.redsnapper.net',
            'ipv4' => '0.0.0.0',
            'active' => 'on',
            'notes' => 'Lorum Ipsum ...'
        ], $overrides);
    }

    private function prepareDatabaseForTesting($overrides = [])
    {
        return $this->post(route('boxes.store'), $this->validParams($overrides));
    }

    /** @test */
    public function should_be_able_to_create_a_box()
    {
        // create lan for box to be assigned to
        Factory(Lan::class)->create();
        // Create Box
        $this->prepareDatabaseForTesting()->assertStatus(302);
        // Get first Box
        if($box = Box::first()){
            // Test Box values
            $this->assertEquals($box->boxname, "TestBox");
            $this->assertEquals($box->lan, 1);
            $this->assertEquals($box->fqdn, "test-box.redsnapper.net");
            $this->assertEquals($box->ipv4, "0.0.0.0");
            $this->assertTrue($box->active);
            $this->assertEquals($box->notes, "Lorum Ipsum ...");
        } else {
            $this->fail('Box was not created');
        }
    }

    /** @test */
    public function boxname_is_required_when_creating_box()
    {
        $this->prepareDatabaseForTesting(['boxname' => ''])
            ->assertSessionHasErrors('boxname');
    }

    /** @test */
    public function boxname_must_be_unique_when_creating_box()
    {
        Factory(Lan::class)->create();
        Factory(Box::class)->create(['boxname'=>'notunique']);
        $this->prepareDatabaseForTesting(['boxname'=>'notunique'])
            ->assertSessionHasErrors('boxname');
    }

    /** @test */
    public function lan_is_required_when_creating_box()
    {
        $this->prepareDatabaseForTesting(['lan' => null])
            ->assertSessionHasErrors('lan');
    }

    /** @test */
    public function lan_must_be_the_id_of_a_real_lan_when_creating_box()
    {
        Factory(Lan::class)->create();

        $this->prepareDatabaseForTesting(['lan' => 1])
            ->assertStatus(302);

        $this->prepareDatabaseForTesting(['lan' => 2])
            ->assertSessionHasErrors('lan');
    }

    /** @test */
    public function fqdn_is_required_when_creating_box()
    {
        $this->prepareDatabaseForTesting(['fqdn' => ''])
            ->assertSessionHasErrors('fqdn');
    }

}