<?php

namespace Tests\Feature;

use App\Box;
use App\Lan;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EditBoxTest extends TestCase
{

    use RefreshDatabase;

    protected function validParams($overrides = []): array
    {
        return array_merge([
            'boxname' => 'TestBox',
            'lan' => 1,
            'fqdn' => 'test-box.redsnapper.net',
            'ipv4' => '0.0.0.0',
            'active' => 'on',
            'notes' => 'Lorum Ipsum ...'
        ], $overrides);
    }

    private function prepareDatabaseForTesting($newParams = [])
    {
        $box = Box::create($this->validParams());
        return $this->patch(route('boxes.update', $box), $newParams);
    }

    /** @test */
    public function should_be_able_to_update_a_action()
    {
        // create lans for box to be assigned to
        Factory(Lan::class)->create();
        Factory(Lan::class)->create();
        // Create and Edit Action
        $this->prepareDatabaseForTesting([
            'boxname' => 'NewBoxName',
            'lan' => 2,
            'fqdn' => 'test-box-new.redsnapper.net',
            'ipv4' => '0.0.10.4',
            'active' => '',
            'notes' => 'Lorum Ipsum Edited...'
        ])->assertStatus(302);
        // Get first Box
        if($editedBox = Box::first()){
            // Test Box values
            $this->assertEquals($editedBox->boxname, "NewBoxName");
            $this->assertEquals($editedBox->lan, 2);
            $this->assertEquals($editedBox->fqdn, "test-box-new.redsnapper.net");
            $this->assertEquals($editedBox->ipv4, "0.0.10.4");
            $this->assertFalse($editedBox->active);
            $this->assertEquals($editedBox->notes, "Lorum Ipsum Edited...");
        } else {
            $this->fail('Box was not created');
        }
    }

    /** @test */
    public function boxname_is_required_when_editing_box()
    {
        $this->prepareDatabaseForTesting(['boxname' => ''])
            ->assertSessionHasErrors('boxname');
    }

    /** @test */
    public function boxname_must_be_unique_when_editing_box()
    {
        Factory(Lan::class)->create();
        Factory(Box::class)->create(['boxname'=>'notunique']);
        $this->prepareDatabaseForTesting(['boxname'=>'notunique'])
            ->assertSessionHasErrors('boxname');
    }

    /** @test */
    public function lan_is_required_when_editing_box()
    {
        $this->prepareDatabaseForTesting(['lan' => null])
            ->assertSessionHasErrors('lan');
    }

    /** @test */
    public function lan_must_be_the_id_of_a_real_lan_when_editing_box()
    {
        Factory(Lan::class)->create();

        $this->prepareDatabaseForTesting(['lan' => 2])
            ->assertStatus(302);
    }

    /** @test */
    public function fqdn_is_required_when_editing_box()
    {
        $this->prepareDatabaseForTesting(['fqdn' => ''])
            ->assertSessionHasErrors('fqdn');
    }

}