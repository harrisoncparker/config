<?php

namespace Tests\Feature;

use App\Box;
use App\Lan;
use Faker\Factory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewBoxTest extends TestCase
{

    use RefreshDatabase;

    protected function validParams($overrides = []): array
    {
        return array_merge([
            'boxname' => 'TestBox',
            'lan' => 1,
            'fqdn' => 'test-box.redsnapper.net',
            'ipv4' => '0.0.0.0',
            'active' => 'on',
            'notes' => 'Lorum Ipsum ...'
        ], $overrides);
    }

    /** @test */
    public function can_view_list_of_boxes()
    {
        // Create Parent Lan with lanid of 1
        $lan = Factory(Lan::class)->create();
        // Create Boxes
        Box::create($this->validParams());
        Box::create($this->validParams([
            'boxname' => 'TestBox2',
            'fqdn' => 'test-box-2.redsnapper.net',
            'ipv4' => '1.2.3.4',
            'active' => ''
        ]));
        // Get response
        $response = $this->get(route('boxes.index'));
        $response->assertStatus(200);
        // Check if we can see Box 1
        $response->assertSee('TestBox');
        $response->assertSee($lan->lanname);
        $response->assertSee('test-box.redsnapper.net');
        $response->assertSee('0.0.0.0');
        $response->assertSee('on');
        // Check if we can see Box 2
        $response->assertSee('TestBox2');
        $response->assertSee('test-box-2.redsnapper.net');
        $response->assertSee('1.2.3.4');
    }

    /** @test */
    function user_can_view_box_create_form()
    {
        $response = $this->get(route('boxes.create'));
        $response->assertStatus(200, 'View create form failed');
        $response->assertSee('Create Box');
        $response->assertSee('<form');
    }

    /** @test */
    function user_can_view_box_edit_form()
    {
        $box = Box::create($this->validParams());
        $response = $this->get(route('boxes.edit',['box' => $box]));
        $response->assertStatus(200);
        $response->assertSee('Edit Box: '. $box->boxname);
        $response->assertSee('<form');
    }

}