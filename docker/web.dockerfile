FROM ubuntu:16.04

ARG XDEBUG_CONFIG

# install php 7.1 repo
RUN	DEBIAN_FRONTEND=noninteractive apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends software-properties-common \
	&& export LANG=C.UTF-8\
	&& DEBIAN_FRONTEND=noninteractive add-apt-repository ppa:ondrej/php


# install all webserver dependencies
RUN 	DEBIAN_FRONTEND=noninteractive apt-get update \
        && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
                apache2 \
                iproute2 \
		vim \
                curl \
                pwgen \
                libssl-dev \
                libmysql++-dev \
                libcurl4-openssl-dev \
                mysql-client libapache2-mod-fcgid \
                libapache2-mod-geoip \
                libfcgi-dev \
                php7.1 \
                php7.1-mysql \
                php7.1-curl \
                php-imagick \
                php7.1-xml \
                php7.1-mbstring \
                libapache2-mod-php7.1 \
                php7.1-zip \
                php7.1-sqlite3 \
                php7.1-xdebug \
		mysql-client \
		libmagickwand-dev \
        && rm -r /var/lib/apt/lists/*

# Copy Config files
COPY ./docker/apacheconf/apache2.conf /etc/apache2/apache2.conf
COPY ./docker/apacheconf/000-default.conf /etc/apache2/sites-available/000-default.conf

COPY ./docker/setupfiles/xdebug.ini /etc/php/7.1/mods-available/xdebug.ini

RUN a2enmod rewrite
RUN a2enmod php7.1

CMD apachectl -D FOREGROUND
