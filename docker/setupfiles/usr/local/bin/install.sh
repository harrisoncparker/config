#!/bin/bash

set -e

# Install Xdebug
if  [ "$INSTALL_XDEBUG" == "true" ]; then
    printf "\nInstalling Xdebug...\n......\n......\n"
    echo "xdebug.remote_host="`/sbin/ip route|awk '/default/ { print $3 }'` >> /tempfiles/xdebug.ini
    apt-get update && apt-get install -y php7.1-xdebug
    mv /tempfiles/xdebug.ini /etc/php/7.1/mods-available/xdebug.ini
fi

# Install Composer
if  [ "$INSTALL_COMPOSER" == "true" ]; then
    cd ~ \
    && curl -sS https://getcomposer.org/installer -o composer-setup.php \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer
fi

# Run composer install and generate key
if  [ "$INSTALL_COMPOSER" == "true" ]; then
    cd /var/www \
    && composer install \
    && php artisan key:generate
fi

#Start apache!
apachectl -DFOREGROUND

