<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** Redirect */
Route::get("/", function () {
    return redirect("lans");
})->name("home");

/** Sites */

/** Boxes */
Route::resource("boxes", "Box\BoxController", ["except" => ["show"]]);
Route::get("boxes/{box}/actions/{action}", "Box\BoxActionController@show")->name("boxes.actions.show");
Route::post("boxes/{box}/actions/{action}/execute", "Box\BoxActionController@execute")->name("boxes.actions.execute");

/** Box Services */
Route::get("boxes/{box}/services", "Box\BoxServiceController@index")->name("boxes.services.index");
Route::resource("boxservices", "Box\BoxServiceController", ["only" => ["store", "update", "destroy"]]);

/** Lans */
Route::resource("lans", "Lan\LanController", ["except" => ["show"]]);
Route::get("lans/{lan}/actions/{action}", "Lan\LanActionController@show")->name("lans.actions.show");
Route::post("lans/{lan}/actions/{action}/execute", "Lan\LanActionController@execute")->name("lans.actions.execute");

/** Services */
Route::resource("services", "Service\ServiceController", ["except" => ["show"]]);

/** Actions */
Route::resource("actions", "Action\ActionController", ["except" => ["show"]]);

/** Modules */




// BG: Added as stubs.
Route::resource("sites", "Site\SiteController", ["except" => ["show"]]);
Route::resource("actions", "Action\ActionController", ["except" => ["show"]]);
Route::resource("modules", "Module\ModuleController", ["except" => ["show"]]);
